import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'login',
    loadChildren: () => import('./views/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'inicio',
    loadChildren: () => import('./views/inicio/inicio.module').then( m => m.InicioPageModule)
  },
  {
    path: 'nuevopedido',
    loadChildren: () => import('./views/nuevopedido/nuevopedido.module').then( m => m.NuevopedidoPageModule)
  },  {
    path: 'departamentos',
    loadChildren: () => import('./views/departamentos/departamentos.module').then( m => m.DepartamentosPageModule)
  },
  {
    path: 'subdepartamentos',
    loadChildren: () => import('./views/subdepartamentos/subdepartamentos.module').then( m => m.SubdepartamentosPageModule)
  },
  {
    path: 'productos-subdepa',
    loadChildren: () => import('./views/productos-subdepa/productos-subdepa.module').then( m => m.ProductosSubdepaPageModule)
  },
  {
    path: 'detalleproducto',
    loadChildren: () => import('./views/detalleproducto/detalleproducto.module').then( m => m.DetalleproductoPageModule)
  },
  {
    path: 'busquedaproducto',
    loadChildren: () => import('./views/busquedaproducto/busquedaproducto.module').then( m => m.BusquedaproductoPageModule)
  },
  {
    path: 'productos-busqueda',
    loadChildren: () => import('./views/productos-busqueda/productos-busqueda.module').then( m => m.ProductosBusquedaPageModule)
  },
  {
    path: 'busquedacliente',
    loadChildren: () => import('./views/busquedacliente/busquedacliente.module').then( m => m.BusquedaclientePageModule)
  },
  {
    path: 'busquedadireccion',
    loadChildren: () => import('./views/busquedadireccion/busquedadireccion.module').then( m => m.BusquedadireccionPageModule)
  },
  {
    path: 'nuevadireccionpedido',
    loadChildren: () => import('./views/nuevadireccionpedido/nuevadireccionpedido.module').then( m => m.NuevadireccionpedidoPageModule)
  },
  {
    path: 'pedidorealizado',
    loadChildren: () => import('./views/pedidorealizado/pedidorealizado.module').then( m => m.PedidorealizadoPageModule)
  },
  {
    path: 'pedidos',
    loadChildren: () => import('./views/pedidos/pedidos.module').then( m => m.PedidosPageModule)
  },
  {
    path: 'detallepedido',
    loadChildren: () => import('./views/detallepedido/detallepedido.module').then( m => m.DetallepedidoPageModule)
  },
  {
    path: 'pickandpacks',
    loadChildren: () => import('./views/pickandpacks/pickandpacks.module').then( m => m.PickandpacksPageModule)
  },
  {
    path: 'nuevopickandpack',
    loadChildren: () => import('./views/nuevopickandpack/nuevopickandpack.module').then( m => m.NuevopickandpackPageModule)
  },
  {
    path: 'nuevopickandpackproductos',
    loadChildren: () => import('./views/nuevopickandpackproductos/nuevopickandpackproductos.module').then( m => m.NuevopickandpackproductosPageModule)
  },
  {
    path: 'busquedamascota',
    loadChildren: () => import('./views/busquedamascota/busquedamascota.module').then( m => m.BusquedamascotaPageModule)
  },
  {
    path: 'nuevamascotapickpack',
    loadChildren: () => import('./views/nuevamascotapickpack/nuevamascotapickpack.module').then( m => m.NuevamascotapickpackPageModule)
  },
  {
    path: 'productospickandpack',
    loadChildren: () => import('./views/productospickandpack/productospickandpack.module').then( m => m.ProductospickandpackPageModule)
  },
  {
    path: 'detallepickpack',
    loadChildren: () => import('./views/detallepickpack/detallepickpack.module').then( m => m.DetallepickpackPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
