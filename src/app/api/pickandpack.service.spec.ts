import { TestBed } from '@angular/core/testing';

import { PickandpackService } from './pickandpack.service';

describe('PickandpackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PickandpackService = TestBed.get(PickandpackService);
    expect(service).toBeTruthy();
  });
});
