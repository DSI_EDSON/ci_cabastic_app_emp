import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PickandpackService {

  private urlBase: string = environment.urlApi + 'pickPack/';

  constructor(public httpClient: HttpClient) { }

  ObtenerCoincidenciasClientes(busqueda) {
    return this.httpClient.post(environment.urlApi + "empleados/obtenerClienteBusqueda", busqueda);
  }

  ObtenerPickAndPackCliente(request) { //Al momento de seleccionar un cliente crea el pickAndPack
    return this.httpClient.post(this.urlBase + "obtenerPickPackCliente", request);
  }

  ObtenerSustanciasPickPack(request) { //obtiene la informacion de un pickpack en curso
    return this.httpClient.post(this.urlBase + "obtenerSustanciasPickPack", request);
  }

  ActualizarDirMasPickPack(request) { //actualiza la direccion  y mascota de un pickpack en curso
    return this.httpClient.post(this.urlBase + "actualizaPickPackCliente", request);
  }

  RegistrarSustanciaPickPack(request) { //registra una sustancia para un pickpack en curso
    return this.httpClient.post(this.urlBase + "registrarSustanciaPickPack", request);
  }
  
  EliminarSustanciaPickPack(request) { //elimina una sustancia para un pickpack en curso
    return this.httpClient.post(this.urlBase + "eliminaSustanciaPickPack", request);
  }

  ActualizarFrecuenciaPickPack(request) { //actualiza la frecuencia de un pickpack en curso
    return this.httpClient.post(this.urlBase + "actualizaPickPackCliente", request);
  }

  RegistraPickPack(request) { //registra un pickpack en curso
    return this.httpClient.post(this.urlBase + "InsertaPickPackERP", request);
  }

  CancelarPickPackEnCurso(request) { //cancela un pickpack en curso
    return this.httpClient.post(this.urlBase + "CancelaPickPack", request);
  }

  CancelarPickPackCerrado(request) { //cancela un pickpack cerrado
    return this.httpClient.post(this.urlBase + "CancelaPickPackCerrado", request);
  }

  ObtenerPickPackCerrados(request) { //obtiene la lista de pickpack creados por el empleado
    return this.httpClient.post(this.urlBase + "obtenerPickPackCerrados", request);
  }

  ObtenerDetallePickPackCerrado(request) { //obtiene el detalle de un pickpack cerrado
    return this.httpClient.post(this.urlBase + "obtenerPickPackCerradosDetalle", request);
  }

}
