import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MascotaService {

  private urlBase: string = environment.urlApi+'mascota/';
  
  constructor(public httpClient: HttpClient) { }

  ObtenerMascotasCliente(request) {
    return this.httpClient.post(this.urlBase + "obtenerMascotasUsuario", request);
  }

  ObtenerTiposMascota(){
    return this.httpClient.get(this.urlBase +"obtenerTipoMascotas");
  }
  
  RegistrarActualizarMascota(post){
    return this.httpClient.post(this.urlBase +"insertarMascotaUsuario",post);
  }
}
