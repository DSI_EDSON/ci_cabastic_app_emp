import { TestBed } from '@angular/core/testing';

import { SubdepartamentoService } from './subdepartamento.service';

describe('SubdepartamentoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubdepartamentoService = TestBed.get(SubdepartamentoService);
    expect(service).toBeTruthy();
  });
});
