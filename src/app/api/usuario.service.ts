import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UsuarioService {

  private urlBase: string = environment.urlApi+'empleados/';


  constructor(public httpClient: HttpClient) { }

  IniciarSesion(usuario){
    return this.httpClient.post(this.urlBase +"login",usuario);
  }

}
