import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService {
  private urlBase: string = environment.urlApi+'departamentos/';
  
  constructor(public httpClient: HttpClient) { }

  ObtenerDepartamentos(){
    return this.httpClient.get(this.urlBase +"ListadoDepartamentos");
  }

}
