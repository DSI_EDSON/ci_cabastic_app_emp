import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SubdepartamentoService {
  private urlBase: string = environment.urlApi+'departamentos/';
  
  constructor(public httpClient: HttpClient) { }

  ObtenerProductos(idSubDepto){
    console.log("idSubDepto: "+JSON.stringify(idSubDepto));
    return this.httpClient.post(environment.urlApi+ "empleados/subDepartamentosDepartamento/productos",idSubDepto);
  }
}
