import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PedidoService {

  private urlBase: string = environment.urlApi + 'empleados/';


  constructor(public httpClient: HttpClient) { }

  ObtenerCoincidenciasClientes(busqueda) {
    return this.httpClient.post(this.urlBase + "obtenerClienteBusqueda", busqueda);
  }

  RegistrarProductoPedidoCliente(request) {
    return this.httpClient.post(environment.urlApi + "empleado/registrarProductoPedidoCliente", request);
  }

  EliminarProductoPedidoCliente(request) {
    return this.httpClient.post(environment.urlApi + "pedido/eliminarProductoPedidoCliente", request);
  }

  ObtenerPedidoCliente(request) { //Al momento de seleccionar un cliente crea el pedido
    return this.httpClient.post(this.urlBase + "obtenerPedidoCliente", request);
  }

  ObtenerDetallePedido(request) { //obtiene el detalle de un pedido generado
    return this.httpClient.post(environment.urlApi + "pedido/obtenerDetallePedidoCliente", request);
  }

  ObtenerInformacionPedidoCliente(request) {
    return this.httpClient.post(this.urlBase + "obtenerInformacionPedidoCliente", request);
  }

  ObtenerDireccionEntrega(request) {
    return this.httpClient.post(this.urlBase + "obtenerDireccionEntregaDetalle", request);
  }

  ObtenerProductosPedidoCliente(request) {
    return this.httpClient.post(this.urlBase + "obtenerProductosPedidoCliente", request);
  }

  ObtenerProductosPedidoRealizadoCliente(request) {
    return this.httpClient.post(environment.urlApi + "pedido/consultarProductosPedido", request);
  }

  ObtenerDireccionesEntregaCliente(request) {
    return this.httpClient.post(environment.urlApi + "direcciones/obtenerDireccionesEntregaCliente", request);
  }

  ActualizarDireccionEntregaCliente(request) {
    return this.httpClient.post(this.urlBase + "actualizarDomicilioPedidoCliente", request);
  }

  RegistrarPedidoCliente(request) {
    return this.httpClient.post(this.urlBase + "cerrarPedidoCliente", request);
  }

  CancelarPedidoAbiertoCliente(request) {
    return this.httpClient.post(environment.urlApi + "pedido/cancelarPedidoAbiertoCliente", request);
  }

  ObtenerPedidosCliente(request) {
    return this.httpClient.post(environment.urlApi + "pedido/obtenerPedidosUsuario", request);
  }

  ObtenerPedidosCerrados(request) {
    return this.httpClient.post(this.urlBase + "obtenerPedidosCerrados", request);
  }

  CancelarPedidoRealizado(request) {
    return this.httpClient.post(environment.urlApi + "pedido/cancelarPedido", request);
  }
  
}
