import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DireccionService {
  private urlBase: string = environment.urlApi+'direcciones/';
  
  constructor(public httpClient: HttpClient) { }

  ObtenerPaises(){
    return this.httpClient.get(this.urlBase +"obtenerPaises");
  }

  ObtenerColonias(request) {
    return this.httpClient.post(this.urlBase + "obtenerColonias", request);
  }

  RegistrarDireccionEntrega(request) {
    return this.httpClient.post(this.urlBase + "registrarDireccionEntrega", request);
  }

  ObtenerDireccionEntrega(request) { // para pedidos o picka and pack
    return this.httpClient.post(environment.urlApi + "empleados/obtenerDireccionEntregaDetalle", request);
  }

}
