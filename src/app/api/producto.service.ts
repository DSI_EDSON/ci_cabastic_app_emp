import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private urlBase: string = environment.urlApi+'productos/';

  constructor(public httpClient: HttpClient) { }

  ObtenerProducto(post){
    return this.httpClient.post(environment.urlApi + "empleados/obtenerDetalleGeneralProducto",post);
  }

  ObtenerBusquedaSugerencia(post){
    return this.httpClient.post(this.urlBase +"obtenerProductosBusquedaSugerencia",post);
  }

  ObtenerProductosBusqueda(post){
    return this.httpClient.post(environment.urlApi+ "empleados/"+"obtenerProductosBusqueda",post);
  }

  ObtenerProductosPickAndPack(){
    return this.httpClient.get(environment.urlApi + "pickPack/obtenerSustancias");
  }

}
