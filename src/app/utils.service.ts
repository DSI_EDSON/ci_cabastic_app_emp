import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Device } from '@ionic-native/device/ngx';


@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  isLoading = false;
  sesionIniciada: boolean = false;
  usuario: any;
  loading;
  /*================    Pedido    ================*/
  pedidoEnCurso: any;
  /*================  Fin Pedido  ================*/

  /*================    Pick and Pack    ================*/
  pickAndPackEnCurso: any;
  /*================  Fin Pick and Pack  ================*/

  constructor(
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public router: Router,
    private storage: Storage,
    public device: Device) { }


  async ShowLoading() {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      message: 'Cargando...'
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async HideLoading() {
    this.isLoading = false;
    return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
  }

  async alert(msj) {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: msj,
      buttons: ['Ok']
    });

    await alert.present();
  }

  /*===============================================================================
  =================================     SESION    =================================  
  ===============================================================================*/
  SetLogin(data) {
    try {
      this.storage.set('usuarioEmp', data);
      this.usuario = data;
    } catch (e) { console.log(e) }
  }

  async ObtenerUsuarioSesion() {
    try {
      this.usuario = await this.GetKeyBD("usuarioEmp");
      return this.usuario;
    } catch (e) { console.log(e) }
  }


  async AlertaCerrarSesion() {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: 'Deseas cerrar tu sesión?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancelo');
          }
        }, {
          text: 'Cerrar sesión',
          handler: () => {
            this.CerrarSesion();
          }
        }
      ]
    });
    await alert.present();
  }

  CerrarSesion() {
    this.SetLogin(null);
    this.SetPedidoEnCurso(null);

    this.router.navigate(['/login'], { replaceUrl: true });
  }

  /*===============================================================================
  =================================     PEDIDO    =================================  
  ===============================================================================*/
  SetPedidoEnCurso(data) {
    try {
      this.storage.set('pedidoEnCursoEmp', data);
      this.pedidoEnCurso = data;
    } catch (e) { console.log(e) }
  }

  async ObtenerPedidoEnCurso() {
    try {
      this.pedidoEnCurso = await this.GetKeyBD("pedidoEnCursoEmp");
      return this.pedidoEnCurso;
    } catch (e) { console.log(e) }
  }

  /*===============================================================================
  =================================     PICK AND PACK    =================================  
  ===============================================================================*/
  SetPickAndPackEnCurso(data) {
    try {
      this.storage.set('PickAndPackEmp', data);
      this.pickAndPackEnCurso = data;
    } catch (e) { console.log(e) }
  }

  async ObtenerPickAndPackEnCurso() {
    try {
      this.pickAndPackEnCurso = await this.GetKeyBD("PickAndPackEmp");
      return this.pickAndPackEnCurso;
    } catch (e) { console.log(e) }
  }

  /*===============================================================================
  =================================     DAO-BD    =================================  
  ===============================================================================*/
  async GetKeyBD(key: string): Promise<void> {
    return await this.storage.get(key);
  }


  ObtenerDatosDispositivo() {
    return this.device.platform != null ?
      this.device.platform + " versión: " + this.device.version + "uuid: " + this.device.uuid :
      "Navegador Web";
  }
}
