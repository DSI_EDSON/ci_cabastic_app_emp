import { Component, OnInit } from '@angular/core';
import { DepartamentoService } from '../../api/departamento.service';
import { Router, NavigationExtras } from '@angular/router';
import { UtilsService } from '../../utils.service';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.page.html',
  styleUrls: ['./departamentos.page.scss'],
})
export class DepartamentosPage implements OnInit {
  departamentosDestacados: any;
  departamentosNoDestacados: any;
  departamentos: any;

  constructor(
    private wsDepartamento: DepartamentoService,
    public router: Router,
    private utils: UtilsService) { }

  ngOnInit() {
    this.ObtenerDepartamentos();
  }


  ObtenerDepartamentos() {
    this.utils.ShowLoading();
    this.wsDepartamento.ObtenerDepartamentos().subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerDepartamentos(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerDepartamentos(error);
    });
  }

  OnSuccessObtenerDepartamentos(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontraron departamentos");
    } else {
      this.departamentos = data;
      this.departamentosDestacados = [];
      this.departamentosDestacados = this.departamentos.filter(word => word.destacada == '1');
      this.departamentosNoDestacados = [];
      this.departamentosNoDestacados = this.departamentos.filter(word => word.destacada == null || word.destacada == '0');
    }
  }

  OnErrorObtenerDepartamentos(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoSubDepartamento(departamento){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        subDepartamentos: JSON.stringify(departamento.subDepartamento),
        nombreDepartamento : departamento.nombre
      }
    };
    this.router.navigate(['subdepartamentos'], navigationExtras);
  }

  GoBuscarProducto(){
    this.router.navigate(['busquedaproducto']);
  }
}
