import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../utils.service';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ProductoService } from '../../api/producto.service';
import { PickandpackService } from '../../api/pickandpack.service';

@Component({
  selector: 'app-productospickandpack',
  templateUrl: './productospickandpack.page.html',
  styleUrls: ['./productospickandpack.page.scss'],
})
export class ProductospickandpackPage implements OnInit {

  pickAndPack: any;
  allProductos:any;
  productos:any;

  txtBusqueda:any;
  mostrarloading:boolean =false;

  constructor(
    private wsProducto: ProductoService,
    private wsPickAndPack: PickandpackService,
    private route: ActivatedRoute, 
    private router: Router,
    private utils: UtilsService,
    private navCtrl: NavController
    ) { }

  ngOnInit() {
    this.LeerParametros();
  }


  LeerParametros(){
    this.route.queryParams.subscribe(params => {
      if (params && params.pickAndPack) {
        this.pickAndPack = JSON.parse(params.pickAndPack);
        this.ObtenerProductos();
      }
    });
  }

  ObtenerProductos(){
    this.utils.ShowLoading();
    this.wsProducto.ObtenerProductosPickAndPack().subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerProductos(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerProductos(error);
    });
  }

  OnSuccessObtenerProductos(data){
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontraron productos, intente mas tarde.");
      this.navCtrl.back();
    } else {
      this.allProductos = data;
      this.OnFiltrarProductos();
    }
  }

  OnErrorObtenerProductos(error){
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  OnFiltrarProductos(){
      if (this.txtBusqueda != null && this.txtBusqueda.trim().length > 0) {
        //ObtenerCoincidenciasClientes(); en caso de ver todos los pedidos de un cliente
        this.FiltrarProductos();
      } else {
        this.productos = this.allProductos;
      }
  }

  FiltrarProductos() {
    this.mostrarloading = true;
    this.productos = this.allProductos.filter(word => word.sustancia.toUpperCase().includes(this.txtBusqueda.toUpperCase()));
    setTimeout(() => {
      this.mostrarloading = false;
    }, 1000);
  }


  /* ============= Eventos  ========== */
  AddProductoPickPack(idSustancia){
    let postData = {
      "idPickPackCliente": this.pickAndPack.idPickPackCliente,
      "idSustancia": idSustancia
    };
    this.utils.ShowLoading();
    this.wsPickAndPack.RegistrarSustanciaPickPack(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessAddProductoPickPack(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorAddProductoPickPack(error);
    });
  }

  OnSuccessAddProductoPickPack(data){
    if (data == null || data.status != 200) {
      this.utils.alert("No se logro añadir el producto al pick and pack, intente mas tarde.");
      this.navCtrl.back();
    } else {
      this.utils.alert("Se agrego correctamente el producto.");
      this.GoNuevoPickAndPackProductos();
    }
  }

  OnErrorAddProductoPickPack(error){
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoNuevoPickAndPackProductos() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        pickAndPack: JSON.stringify(this.pickAndPack)
      }
    };
    this.router.navigate(['nuevopickandpackproductos'], navigationExtras);
  }
}
