import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductospickandpackPage } from './productospickandpack.page';

describe('ProductospickandpackPage', () => {
  let component: ProductospickandpackPage;
  let fixture: ComponentFixture<ProductospickandpackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductospickandpackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductospickandpackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
