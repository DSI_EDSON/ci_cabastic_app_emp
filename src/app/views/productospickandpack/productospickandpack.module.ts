import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductospickandpackPageRoutingModule } from './productospickandpack-routing.module';

import { ProductospickandpackPage } from './productospickandpack.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductospickandpackPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ProductospickandpackPage]
})
export class ProductospickandpackPageModule {}
