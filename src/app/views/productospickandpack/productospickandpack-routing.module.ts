import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductospickandpackPage } from './productospickandpack.page';

const routes: Routes = [
  {
    path: '',
    component: ProductospickandpackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductospickandpackPageRoutingModule {}
