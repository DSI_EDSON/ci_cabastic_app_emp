import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../utils.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  constructor(
    public utils : UtilsService,
    public router : Router) { }

  ngOnInit() {
  }


  GoDepartamentos(){
    this.router.navigate(['/departamentos']);
  }

  GoNuevoPedido(){
    this.router.navigate(['/nuevopedido']);
  }

  GoPedidos(){
    this.router.navigate(['/pedidos']);
  }

  GoPickAndPacks(){
    this.router.navigate(['/pickandpacks']);
  }

  GoCerrarSesion() {
    this.utils.AlertaCerrarSesion();
  }

}
