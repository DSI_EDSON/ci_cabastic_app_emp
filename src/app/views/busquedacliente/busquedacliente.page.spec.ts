import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusquedaclientePage } from './busquedacliente.page';

describe('BusquedaclientePage', () => {
  let component: BusquedaclientePage;
  let fixture: ComponentFixture<BusquedaclientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusquedaclientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusquedaclientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
