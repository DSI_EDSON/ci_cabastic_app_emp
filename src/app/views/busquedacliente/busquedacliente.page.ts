import { Component, OnInit } from '@angular/core';
import { PedidoService } from '../../api/pedido.service';
import { Router, NavigationExtras } from '@angular/router';
import { UtilsService } from '../../utils.service';
@Component({
  selector: 'app-busquedacliente',
  templateUrl: './busquedacliente.page.html',
  styleUrls: ['./busquedacliente.page.scss'],
})
export class BusquedaclientePage implements OnInit {

  txtBusqueda:string;
  mostrarloading: boolean = false;
  clientes: any=[];

  constructor(private wsPedido: PedidoService,
    public router: Router,
    private utils: UtilsService) { }

  ngOnInit() {
  }

  ObtenerCoincidenciasClientes() {
    this.clientes = [];
    if (this.txtBusqueda != '') {
      let postData = {
        "nombre": this.txtBusqueda
      }
      this.mostrarloading = true;
      this.wsPedido.ObtenerCoincidenciasClientes(postData).subscribe(data => {
        this.mostrarloading = false;
        this.OnSuccessObtenerCoincidenciasClientes(data);
      }, error => {
        this.mostrarloading = false;
        this.OnErrorObtenerCoincidenciasClientes(error);
      });
    }
    
  }

  OnSuccessObtenerCoincidenciasClientes(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.clientes = data;
    }
  }

  OnErrorObtenerCoincidenciasClientes(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  OnSeleccionCliente(cliente){
    let postData = {
      "idEmpleado": this.utils.usuario.idUsuario,
      "idCliente": cliente.idCliente
    }
    this.utils.ShowLoading();
    this.wsPedido.ObtenerPedidoCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessSeleccionPedidoCliente(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorSeleccionPedidoCliente(error);
    });
  }

  OnSuccessSeleccionPedidoCliente(data){
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.utils.SetPedidoEnCurso(data);
      this.router.navigate(['/nuevopedido']);
    }
  }

  OnErrorSeleccionPedidoCliente(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

}
