import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusquedaclientePage } from './busquedacliente.page';

const routes: Routes = [
  {
    path: '',
    component: BusquedaclientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusquedaclientePageRoutingModule {}
