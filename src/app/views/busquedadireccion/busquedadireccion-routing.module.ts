import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusquedadireccionPage } from './busquedadireccion.page';

const routes: Routes = [
  {
    path: '',
    component: BusquedadireccionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusquedadireccionPageRoutingModule {}
