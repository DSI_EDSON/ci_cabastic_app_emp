import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusquedadireccionPage } from './busquedadireccion.page';

describe('BusquedadireccionPage', () => {
  let component: BusquedadireccionPage;
  let fixture: ComponentFixture<BusquedadireccionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusquedadireccionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusquedadireccionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
