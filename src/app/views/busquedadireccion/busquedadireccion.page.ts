import { Component, OnInit } from '@angular/core';
import { PedidoService } from '../../api/pedido.service';
import { PickandpackService } from '../../api/pickandpack.service';
import { Router, ActivatedRoute,NavigationExtras } from '@angular/router';
import { UtilsService } from '../../utils.service';

@Component({
  selector: 'app-busquedadireccion',
  templateUrl: './busquedadireccion.page.html',
  styleUrls: ['./busquedadireccion.page.scss'],
})
export class BusquedadireccionPage implements OnInit {

  direcciones: any;
  idDireccionSeleccionada: any;

  pedido: any;
  pickAndPack: any;

  origen: any; // 1.- pedido, 2.- pick and pack,

  constructor(private wsPedido: PedidoService,
    private wsPickPack: PickandpackService,
    public router: Router,
    private route: ActivatedRoute,
    private utils: UtilsService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params && params.pedido) {
        this.pedido = JSON.parse(params.pedido);
        this.ObtenerDireccionesClientes(this.pedido.idCliente);
        this.origen = 1;
      }
      if (params && params.pickAndPack) {
        this.pickAndPack = JSON.parse(params.pickAndPack);
        this.ObtenerDireccionesClientes(this.pickAndPack.idCliente);
        this.origen = 2;
      }
    });
  }

  ObtenerDireccionesClientes(idCliente) {
    this.direcciones = [];
    let postData = {
      "idUsuario": idCliente
    }
    this.utils.ShowLoading();
    this.wsPedido.ObtenerDireccionesEntregaCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerDireccionesClientes(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerDireccionesClientes(error);
    });
  }

  OnSuccessObtenerDireccionesClientes(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      if (data[0].result.status == 200) {
        this.direcciones = data;
      }
    }
  }

  OnErrorObtenerDireccionesClientes(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  OnSelectDireccion(idDireccion) {
    // 1.- pedido, 2.- pick and pack,
    switch (this.origen) {
      case 1:
        this.ActualizarDireccionEntregaPedidoCliente(idDireccion);
        break;
      case 2:
        this.ActualizarDireccionEntregaPickPackCliente(idDireccion);
        break;
      default:
        break;
    }


  }

  /*  ===============  pedido =============== */
  ActualizarDireccionEntregaPedidoCliente(idDireccion) {
    let postData = {
      "idPedidoCliente": this.pedido.idPedidoCliente,
      "idDomicilio": idDireccion
    }
    this.utils.ShowLoading();
    this.wsPedido.ActualizarDireccionEntregaCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessActualizarDireccionEntregaPedidoCliente(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorActualizarDireccionEntregaPedidoCliente(error);
    });
  }

  OnSuccessActualizarDireccionEntregaPedidoCliente(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("No fue posible cambiar la dirección de entrega, intente mas tarde.");
    } else {
      if (data.status == 200) {
        this.router.navigate(['/nuevopedido']);
      } else {
        this.utils.alert("No fue posible cambiar la dirección de entrega, intente mas tarde.");
      }
    }
  }

  OnErrorActualizarDireccionEntregaPedidoCliente(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  /*  ===============  pick and pack =============== */
  ActualizarDireccionEntregaPickPackCliente(idDireccion) {
    let postData = {
      "idPickPackCliente": this.pickAndPack.idPickPackCliente,
      "idDomicilio": idDireccion,
      "idMascota" : "0",
      "entregaMensual": this.pickAndPack.entregaMensual,
      "diaEntrega": this.pickAndPack.diaEntrega,
      "entregaUnica": this.pickAndPack.entregaUnica,
    }
    //console.log(postData);
    this.utils.ShowLoading();
    this.wsPickPack.ActualizarDirMasPickPack(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessActualizarDireccionEntregaPickPackCliente(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorActualizarDireccionEntregaPickPackCliente(error);
    });
  }

  OnSuccessActualizarDireccionEntregaPickPackCliente(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("No fue posible cambiar la dirección de entrega, intente mas tarde.");
    } else {
      if (data.status == 200) {
        this.router.navigate(['/nuevopickandpack']);
      } else {
        this.utils.alert("No fue posible cambiar la dirección de entrega, intente mas tarde.");
      }
    }
  }

  OnErrorActualizarDireccionEntregaPickPackCliente(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoNuevaDireccionPedido() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        idCliente: (this.origen == 1 ? this.pedido.idCliente : this.pickAndPack.idCliente)
      }
    };
    this.router.navigate(['nuevadireccionpedido'], navigationExtras);
  }

}
