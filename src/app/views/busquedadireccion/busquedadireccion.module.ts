import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusquedadireccionPageRoutingModule } from './busquedadireccion-routing.module';

import { BusquedadireccionPage } from './busquedadireccion.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusquedadireccionPageRoutingModule,
    ComponentsModule
  ],
  declarations: [BusquedadireccionPage]
})
export class BusquedadireccionPageModule {}
