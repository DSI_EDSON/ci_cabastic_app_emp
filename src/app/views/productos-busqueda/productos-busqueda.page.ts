import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProductoService } from '../../api/producto.service';
import { UtilsService } from '../../utils.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-productos-busqueda',
  templateUrl: './productos-busqueda.page.html',
  styleUrls: ['./productos-busqueda.page.scss'],
})
export class ProductosBusquedaPage implements OnInit {

  palabraCoincidencia : any;
  productos:any;

  constructor(
    private wsProducto:ProductoService,
    private route: ActivatedRoute, 
    private router: Router,
    private utils: UtilsService,
    private navCtrl: NavController
    ) { }

  ngOnInit() {
    this.LeerParametros();
  }

  LeerParametros(){
    this.route.queryParams.subscribe(params => {
      if (params && params.palabraCoincidencia) {
        this.palabraCoincidencia = params.palabraCoincidencia;
        //console.log("***"+JSON.stringify(this.subDepartamento));
        this.ObtenerProductos(this.palabraCoincidencia);
      }
    });
  }

  ObtenerProductos(palabraCoincidencia){
    this.utils.ShowLoading();
    let postData = {"palabraClave": palabraCoincidencia}
    this.wsProducto.ObtenerProductosBusqueda(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerProductos(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerProductos(error);
    });
  }

  OnSuccessObtenerProductos(data){
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontraron productos, intente mas tarde.");
      this.navCtrl.back();
    } else {
      this.productos = data;
      console.log(data);
    }
  }

  OnErrorObtenerProductos(error){
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoDetalleProducto(id){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        idProducto: id
      }
    };
    this.router.navigate(['detalleproducto'], navigationExtras);
  }

}
