import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductosBusquedaPage } from './productos-busqueda.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosBusquedaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosBusquedaPageRoutingModule {}
