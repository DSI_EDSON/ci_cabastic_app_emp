import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductosBusquedaPageRoutingModule } from './productos-busqueda-routing.module';

import { ProductosBusquedaPage } from './productos-busqueda.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductosBusquedaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ProductosBusquedaPage]
})
export class ProductosBusquedaPageModule {}
