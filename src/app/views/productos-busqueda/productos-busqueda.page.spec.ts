import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductosBusquedaPage } from './productos-busqueda.page';

describe('ProductosBusquedaPage', () => {
  let component: ProductosBusquedaPage;
  let fixture: ComponentFixture<ProductosBusquedaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductosBusquedaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductosBusquedaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
