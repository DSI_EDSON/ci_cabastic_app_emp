import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubdepartamentosPageRoutingModule } from './subdepartamentos-routing.module';

import { SubdepartamentosPage } from './subdepartamentos.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubdepartamentosPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SubdepartamentosPage]
})
export class SubdepartamentosPageModule {}
