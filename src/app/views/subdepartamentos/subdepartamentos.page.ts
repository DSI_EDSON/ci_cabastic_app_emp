import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-subdepartamentos',
  templateUrl: './subdepartamentos.page.html',
  styleUrls: ['./subdepartamentos.page.scss'],
})
export class SubdepartamentosPage implements OnInit {

  subDepartamentos : any;
  nombreDepartamento : any;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params && params.subDepartamentos) {
        this.subDepartamentos = JSON.parse(params.subDepartamentos);
        this.nombreDepartamento = params.nombreDepartamento;
      }
    });
  }


  GoProductosSubDepa(subDepa){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        subDepartamento: JSON.stringify(subDepa),
        nombreDepartamento: this.nombreDepartamento
      }
    };
    this.router.navigate(['productos-subdepa'], navigationExtras);
  }
}
