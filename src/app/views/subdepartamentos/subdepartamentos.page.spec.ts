import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SubdepartamentosPage } from './subdepartamentos.page';

describe('SubdepartamentosPage', () => {
  let component: SubdepartamentosPage;
  let fixture: ComponentFixture<SubdepartamentosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubdepartamentosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SubdepartamentosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
