import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubdepartamentosPage } from './subdepartamentos.page';

const routes: Routes = [
  {
    path: '',
    component: SubdepartamentosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubdepartamentosPageRoutingModule {}
