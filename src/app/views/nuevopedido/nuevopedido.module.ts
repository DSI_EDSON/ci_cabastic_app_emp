import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevopedidoPageRoutingModule } from './nuevopedido-routing.module';

import { NuevopedidoPage } from './nuevopedido.page';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevopedidoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [NuevopedidoPage]
})
export class NuevopedidoPageModule {}
