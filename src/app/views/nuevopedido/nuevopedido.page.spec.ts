import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NuevopedidoPage } from './nuevopedido.page';

describe('NuevopedidoPage', () => {
  let component: NuevopedidoPage;
  let fixture: ComponentFixture<NuevopedidoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevopedidoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NuevopedidoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
