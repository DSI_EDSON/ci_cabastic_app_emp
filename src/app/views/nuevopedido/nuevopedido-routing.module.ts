import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevopedidoPage } from './nuevopedido.page';

const routes: Routes = [
  {
    path: '',
    component: NuevopedidoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevopedidoPageRoutingModule {}
