import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { UtilsService } from '../../utils.service';
import { PedidoService } from '../../api/pedido.service';
import { Platform, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-nuevopedido',
  templateUrl: './nuevopedido.page.html',
  styleUrls: ['./nuevopedido.page.scss'],
})
export class NuevopedidoPage implements OnInit {

  pedidoEnCurso: boolean = false;

  txtBusqueda: string;
  mostrarloading: boolean = false;
  clientes: any = [];

  pedido: any;
  cliente: any;
  direccion: any;
  productos: any;

  costoTotal: any;
  costoTotalPesos: any;

  constructor(
    public router: Router,
    private wsPedido: PedidoService,
    private utils: UtilsService,
    private platform: Platform,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    /*if(this.pedido == null){
      console.log("ngOnInit");
      this.LoadPedidoEnCurso();
    }*/
    /*this.platform.ready().then(() => {
      if (Device != null) {
        console.log('Device UUID is: ' + JSON.stringify(this.device.uuid) + "version: " + this.device.version);
        alert("Plataforma "+ this.device.platform+" UUID: "+ this.device.uuid+ " version: " + this.device.version);
      }
     
    });
    */
  }

  ionViewWillEnter() {
    if (this.utils.pedidoEnCurso != null) {
      this.pedidoEnCurso = true;
      this.ObtenerPedidoCliente();
    }
  }

  async LoadPedidoEnCurso() {
    this.pedido = await this.utils.ObtenerPedidoEnCurso();
    if (this.pedido != null) {
      //console.log("-----Pedido en curso" + JSON.stringify(this.pedido));
      this.pedidoEnCurso = true;
      this.ObtenerPedidoCliente();
    } else {
      this.pedidoEnCurso = false;
    }
  }

  ObtenerPedidoCliente() {
    let postData = {
      "idEmpleado": this.utils.usuario.idUsuario,
      "idCliente": this.utils.pedidoEnCurso.idCliente
    }
    this.utils.ShowLoading();
    this.wsPedido.ObtenerPedidoCliente(postData).subscribe(data => {
      this.OnSuccessObtenerDatosPedido(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerDatosPedido(error);
    });
  }

  OnSuccessObtenerDatosPedido(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log(JSON.stringify(data));
      this.pedido = data;
      this.utils.SetPedidoEnCurso(this.pedido);
      this.MostrarDatosPedido();
    }
  }

  OnErrorObtenerDatosPedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  MostrarDatosPedido() {
    this.ObtenerCliente();
    this.ObtenerInformacionPedido();
    //Se obtiene los datos de la direccion de entrega
    if (this.pedido.idDomicilio > 0)
      this.ObtenerDireccionEntrega();
    this.ObtenerProductosPedido();
  }

  ObtenerCliente() {
    this.cliente = {};
    this.cliente.nombres = this.pedido.nombres;
    this.cliente.correo = this.pedido.correo;
    this.cliente.celular = this.pedido.celular;
  }

  ObtenerInformacionPedido() {
    let postData = {
      "idPedidoCliente": this.pedido.idPedidoCliente
    }
    this.wsPedido.ObtenerInformacionPedidoCliente(postData).subscribe(data => {
      this.OnSuccessObtenerInformacionPedido(data);
    }, error => {
      this.OnErrorObtenerInformacionPedido(error);
    });
  }

  OnSuccessObtenerInformacionPedido(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log(JSON.stringify(data));
      this.costoTotal = data.costoTotal;
      this.costoTotalPesos = data.costoTotalPesos;
    }
  }

  OnErrorObtenerInformacionPedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  ObtenerDireccionEntrega() {
    let postData = {
      "idDomicilio": this.pedido.idDomicilio
    }
    this.wsPedido.ObtenerDireccionEntrega(postData).subscribe(data => {
      this.OnSuccessDireccionEntrega(data);
    }, error => {
      this.OnErrorDireccionEntrega(error);
    });
  }

  OnSuccessDireccionEntrega(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.direccion = data;
      //console.log(JSON.stringify(data));
    }
  }

  OnErrorDireccionEntrega(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  ObtenerProductosPedido() {
    let postData = {
      "idPedidoCliente": this.pedido.idPedidoCliente
    }
    this.wsPedido.ObtenerProductosPedidoCliente(postData).subscribe(data => {
      this.OnSuccessObtenerProductosPedido(data);
      this.utils.HideLoading();
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerProductosPedido(error);
    });
  }

  OnSuccessObtenerProductosPedido(data) {
    if (data == null || data.length == 0) {
      this.productos = null;
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.productos = data;
      //console.log("Productos:::::" + JSON.stringify(data));
    }
  }

  OnErrorObtenerProductosPedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  //===================== EVENTOS =====================

  ObtenerCoincidenciasClientes() {
    this.clientes = [];
    if (this.txtBusqueda != '') {
      let postData = {
        "nombre": this.txtBusqueda
      }
      this.mostrarloading = true;
      this.wsPedido.ObtenerCoincidenciasClientes(postData).subscribe(data => {
        this.mostrarloading = false;
        this.OnSuccessObtenerCoincidenciasClientes(data);
      }, error => {
        this.mostrarloading = false;
        this.OnErrorObtenerCoincidenciasClientes(error);
      });
    }

  }

  OnSuccessObtenerCoincidenciasClientes(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.clientes = data;
    }
  }

  OnErrorObtenerCoincidenciasClientes(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  OnSeleccionCliente(cliente) {
    let postData = {
      "idEmpleado": this.utils.usuario.idUsuario,
      "idCliente": cliente.idCliente
    }
    this.utils.ShowLoading();
    this.wsPedido.ObtenerPedidoCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessSeleccionPedidoCliente(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorSeleccionPedidoCliente(error);
    });
  }

  OnSuccessSeleccionPedidoCliente(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("Ocurrio un problema, intente mas tarde.");
    } else {
      this.utils.SetPedidoEnCurso(data);
      this.pedidoEnCurso = true;
      this.clientes = [];
      this.ObtenerPedidoCliente();
    }
  }

  OnErrorSeleccionPedidoCliente(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  CambiarCantidadPiezasProducto(item) {
    this.utils.ShowLoading();
    let postData = {
      "idProducto": item.idProducto,
      "idTalla": item.idTalla,
      "idColor": item.idColor,
      "cantidad": item.cantidad,
      "idEmpleado": this.utils.usuario.idUsuario,
      "idPedidoCliente": this.pedido.idPedidoCliente
    };
    this.wsPedido.RegistrarProductoPedidoCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessCambiarCantidadPiezasProducto(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorCambiarCantidadPiezasProducto(error);
    });
  }

  OnSuccessCambiarCantidadPiezasProducto(data) {
    if (data == null || data.status != 200) {
      this.utils.alert("No se logro cambiar la cantidad del producto, intente mas tarde.");
    } else {
      this.utils.alert("Se cambio correctamente la cantidad del producto.");
      this.ObtenerInformacionPedido();
      this.ObtenerProductosPedido();
    }
  }

  OnErrorCambiarCantidadPiezasProducto(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  async AlertaEliminarProducto(producto: any) {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: 'Desea eliminar el producto: <b>' + producto.nombre_producto + '</b>',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancelo');
          }
        }, {
          text: 'Eliminar producto',
          handler: () => {
            this.EliminarProducto(producto);
          }
        }
      ]
    });
    await alert.present();
  }

  EliminarProducto(producto) {
    this.utils.ShowLoading();
    let posData = {
      "idRegistro": producto.idRegistro
    };
    this.wsPedido.EliminarProductoPedidoCliente(posData).subscribe(data => {
      this.OnSuccessEliminarProducto(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorEliminarProducto(error);
    });
  }

  OnSuccessEliminarProducto(data) {
    this.ObtenerInformacionPedido();
    this.ObtenerProductosPedido();
    this.utils.alert('Se ha eliminado el producto exitosamente.');
  }

  OnErrorEliminarProducto(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  OnRelizarPedido() {
    if (this.pedido != null) {
      if (this.productos.length <= 0) {
        this.utils.alert("Agregue por lo menos un producto para su pedido.");
        return;
      }
      if (this.direccion == null) {
        this.utils.alert("Seleccione una dirección de entrega");
      } else {
        //alert("realizar pedido  ");
        this.AlertaRealizarPedido();
      }
    } else {
      this.utils.alert("Ocurrio un problema, intente mas tarde.");
    }
  }

  async AlertaRealizarPedido() {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: 'Realmente desea realizar el pedido?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancelo');
          }
        }, {
          text: 'Realizar el pedido',
          handler: () => {
            this.RelizarPedido();
          }
        }
      ]
    });
    await alert.present();
  }

  RelizarPedido() {
    let postData = {
      "idPedidoCliente": this.pedido.idPedidoCliente,
      "informacionDispositivo": this.utils.ObtenerDatosDispositivo()
    }
    this.utils.ShowLoading();
    this.wsPedido.RegistrarPedidoCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessRelizarPedido(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerRelizarPedido(error);
    });
  }

  OnSuccessRelizarPedido(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log("Pedido Realizado:::::" + JSON.stringify(data));
      if (data.status == 200) {
        this.GoPedidoRealizado(data.idPedido);
        this.utils.SetPedidoEnCurso(null);
      }
      else {
        this.utils.alert(data.mensaje);
      }
    }
  }

  OnErrorObtenerRelizarPedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  async AlertaCancelarPedido() {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: 'Realmente desea cancelar el pedido en curso?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancelo');
          }
        }, {
          text: 'Cancelar pedido',
          handler: () => {
            this.CancelarPedido();
          }
        }
      ]
    });
    await alert.present();
  }

  CancelarPedido() {
    let postData = {
      "idPedido": this.pedido.idPedidoCliente,
    }
    this.utils.ShowLoading();
    this.wsPedido.CancelarPedidoAbiertoCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessCancelarPedido(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorCancelarPedido(error);
    });
  }

  OnSuccessCancelarPedido(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      console.log("Pedido Cancelado:::::" + JSON.stringify(data));
      if (data.status == 200) {
        this.utils.SetPedidoEnCurso(null);
        this.router.navigate(['inicio'], { replaceUrl: true });
      }
      else {
        this.utils.alert(data.mensaje);
      }
    }
  }

  OnErrorCancelarPedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoDepartamentos() {
    this.router.navigate(['/departamentos']);
  }

  GoBusquedaCliente() {
    this.router.navigate(['/busquedacliente'])
  }

  GoBusquedaDireccion() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        pedido: JSON.stringify(this.pedido)
      }
    };
    this.router.navigate(['busquedadireccion'], navigationExtras);
  }

  GoPedidoRealizado(idPedido_) {
    /*let navigationExtras: NavigationExtras = {
      queryParams: {
        idPedido: idPedido_,
        idCliente: this.pedido.idCliente
      }
    };*/
    this.router.navigate(['pedidorealizado'], {
      replaceUrl: true,
      queryParams: {
        idPedido: idPedido_,
        idCliente: this.pedido.idCliente
      }
    });
  }

}
