import { Component, OnInit } from '@angular/core';
import { PedidoService } from '../../api/pedido.service';
import { Router, NavigationExtras } from '@angular/router';
import { UtilsService } from '../../utils.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {

  permitirBusqueda: boolean = true;
  txtBusqueda: string;
  mostrarloading: boolean = false;
  clientes: any = [];
  clienteSeleccionado: any;
  pedidos: any;
  allPedidos: any;

  constructor(private wsPedido: PedidoService,
    public router: Router,
    private utils: UtilsService) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.ObtenerPedidosCerrados();
  }

  ObtenerPedidosCerrados() {
    let postData = {
      "idEmpleado": this.utils.usuario.idUsuario
    }
    this.mostrarloading = true;
    this.wsPedido.ObtenerPedidosCerrados(postData).subscribe(data => {
      this.mostrarloading = false;
      this.OnSuccessObtenerPedidosCerrados(data);
    }, error => {
      this.mostrarloading = false;
      this.OnErrorObtenerPedidosCerrados(error);
    });
  }

  OnSuccessObtenerPedidosCerrados(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontraron pedidos");
    } else {
      this.allPedidos = data;
      this.OnFiltrarPedidosClientes();
      //console.log("Pedidos cerrados: " + JSON.stringify(data));
    }
  }

  OnErrorObtenerPedidosCerrados(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  OnFiltrarPedidosClientes() {
    if (this.permitirBusqueda) {
      if (this.txtBusqueda != null && this.txtBusqueda.trim().length > 0) {
        //ObtenerCoincidenciasClientes(); en caso de ver todos los pedidos de un cliente
        this.FiltrarPedidosClientes();
      } else {
        this.pedidos = this.allPedidos;
      }
      this.ObtenerColores(); // comentar en caso de ver todos los pedidos de un cliente
    }
  }

  FiltrarPedidosClientes() {
    this.mostrarloading = true;
    this.pedidos = this.allPedidos.filter(word => word.nombres.toUpperCase().includes(this.txtBusqueda.toUpperCase()));
    setTimeout(() => {
      this.mostrarloading = false;
    }, 1000);
  }


/*======================================  EN CASO DE QUE NO QUIERAN TODOS LOS PEDIDOS DE UN CLIENTE SE ELIMINA LO DE ABAJO =========== */

  ObtenerCoincidenciasClientes() {
    this.clientes = [];
    this.pedidos = [];
    let postData = {
      "nombre": this.txtBusqueda
    }
    this.mostrarloading = true;
    this.wsPedido.ObtenerCoincidenciasClientes(postData).subscribe(data => {
      this.mostrarloading = false;
      this.OnSuccessObtenerCoincidenciasClientes(data);
    }, error => {
      this.mostrarloading = false;
      this.OnErrorObtenerCoincidenciasClientes(error);
    });
  }

  OnSuccessObtenerCoincidenciasClientes(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.clientes = data;
    }
  }

  OnErrorObtenerCoincidenciasClientes(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  ObtenerPedidosCliente(cliente) {
    this.clientes = [];
    this.clienteSeleccionado = cliente;
    this.permitirBusqueda = false;
    this.txtBusqueda = cliente.nombre;
    ///----------------
    let postData = {
      "idUsuario": cliente.idCliente
    }
    this.utils.ShowLoading();
    this.wsPedido.ObtenerPedidosCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerPedidosCliente(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerPedidosCliente(error);
    });
  }

  OnSuccessObtenerPedidosCliente(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontraron pedidos del cliente: " + this.clienteSeleccionado.nombre);
    } else {
      console.log("pedidos: " + JSON.stringify(data));
      this.pedidos = data;
      this.ObtenerColores();
    }
    setTimeout(() => {
      this.permitirBusqueda = true;
    }, 1000);
  }

  OnErrorObtenerPedidosCliente(error) {
    setTimeout(() => {
      this.permitirBusqueda = true;
    }, 1000);
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  ObtenerColores() {
    this.pedidos.filter(word => word.idStatusPedido == 1).map(function (x) {
      x.color = "green";
    });
    this.pedidos.filter(word => word.idStatusPedido == 3).map(function (x) {
      x.color = "orange";
    });
    this.pedidos.filter(word => word.idStatusPedido == 7).map(function (x) {
      x.color = "red";
    });
    this.pedidos.filter(word => word.idStatusPedido != 1 && word.idStatusPedido != 3 && word.idStatusPedido != 7).map(function (x) {
      x.color = "blue";
    });
  }

  GoDetallePedido(pedido){
    //console.log(JSON.stringify(pedido));
    let navigationExtras: NavigationExtras = {
      queryParams: {
        idPedido: pedido.idPedidoCliente,
        idCliente: pedido.idCliente
      }
    };
    this.router.navigate(['detallepedido'], navigationExtras);
  }

}
