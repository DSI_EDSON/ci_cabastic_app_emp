import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilsService } from '../../utils.service';
import { PedidoService } from '../../api/pedido.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detallepedido',
  templateUrl: './detallepedido.page.html',
  styleUrls: ['./detallepedido.page.scss'],
})
export class DetallepedidoPage implements OnInit {


  pedido: any = [];
  idPedido:any;
  idCliente: any;
  productos:any;
  colorEstatus = "green";

  constructor(
    public router: Router,
    private wsPedido: PedidoService,
    private utils: UtilsService,
    private route: ActivatedRoute,
    private alertCtrl:AlertController
  ) { }

  ngOnInit() {
    this.LeerParametros();
  }


  LeerParametros() {
    this.route.queryParams.subscribe(params => {
      if (params && params.idPedido) {
        this.idPedido = params.idPedido;
        this.idCliente = params.idCliente;
        this.ObtenerDetallePedido();
      }
    });
  }

  ObtenerDetallePedido(){
    let postData = {
      "idCliente": this.idCliente,
      "idPedido" : this.idPedido
    }
    this.utils.ShowLoading();
    this.wsPedido.ObtenerDetallePedido(postData).subscribe(data => {
      this.OnSuccessObtenerDetallePedido(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerDetallePedido(error);
    });
  }

  OnSuccessObtenerDetallePedido(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontro el pedido seleccionado");
    } else {
      this.pedido = data;
      this.ObtenerColorEstatusPedido();
      this.ObtenerProductosPedido();
    }
  }

  OnErrorObtenerDetallePedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  
  ObtenerProductosPedido() {
    let postData = {
      "idPedido": this.pedido.idPedido
    }
    this.wsPedido.ObtenerProductosPedidoRealizadoCliente(postData).subscribe(data => {
      this.OnSuccessObtenerProductosPedido(data);
      this.utils.HideLoading();
    }, error => {
      this.OnErrorObtenerProductosPedido(error);
    });
  }

  OnSuccessObtenerProductosPedido(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.productos = data;
      //console.log("Productos:::::" + JSON.stringify(data));
    }
  }

  OnErrorObtenerProductosPedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  ObtenerColorEstatusPedido(){
    if(this.pedido.idStatusPedido == 1){
      this.colorEstatus = "green";
    };
    if(this.pedido.idStatusPedido == 3){
      this.colorEstatus = "orange";
    };
    if(this.pedido.idStatusPedido == 7){
      this.colorEstatus = "red";
    };
    if(this.pedido.idStatusPedido != 1 && this.pedido.idStatusPedido != 3 && this.pedido.idStatusPedido != 7){
      this.colorEstatus = "blue";
    };
  }

  async AlertaCancelarPedido() {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: 'Realmente desea cancelar el pedido?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancelo');
          }
        }, {
          text: 'Cancelar pedido',
          handler: () => {
            this.CancelarPedido();
          }
        }
      ]
    });
    await alert.present();
  }

  CancelarPedido() {
    let postData = {
      "idPedido": this.pedido.idPedido,
    }
    this.utils.ShowLoading();
    this.wsPedido.CancelarPedidoRealizado(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessCancelarPedido(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorCancelarPedido(error);
    });
  }

  OnSuccessCancelarPedido(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log("Pedido Cancelado:::::" + JSON.stringify(data));
      if (data.status == 200) {
        this.utils.alert("Pedido cancelado exitosamente.");
        this.router.navigate(['pedidos'], { replaceUrl: true });
      } 
      else {
        this.utils.alert(data.mensaje);
      }
    }
  }

  OnErrorCancelarPedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


}
