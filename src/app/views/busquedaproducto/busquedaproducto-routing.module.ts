import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusquedaproductoPage } from './busquedaproducto.page';

const routes: Routes = [
  {
    path: '',
    component: BusquedaproductoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusquedaproductoPageRoutingModule {}
