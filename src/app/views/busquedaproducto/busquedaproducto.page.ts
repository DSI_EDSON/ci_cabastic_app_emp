import { Component, OnInit, ElementRef, ViewChild, ViewChildren } from '@angular/core';
import { ProductoService } from '../../api/producto.service';
import { Router, NavigationExtras } from '@angular/router';
import { UtilsService } from '../../utils.service';

@Component({
  selector: 'app-busquedaproducto',
  templateUrl: './busquedaproducto.page.html',
  styleUrls: ['./busquedaproducto.page.scss'],
})

export class BusquedaproductoPage implements OnInit {

  @ViewChildren("txtBusqueda_") _el: ElementRef;

  txtBusqueda: string;
  productos: any;
  mostrarloading: boolean = false;

  constructor(
    private wsProducto: ProductoService,
    public router: Router,
    private utils: UtilsService) { }

  ngOnInit() {
  }

  ObtenerCoincidenciasProductos() {
    this.productos = [];
    if (this.txtBusqueda != '') {
      let postData = {
        "palabraClave": this.txtBusqueda,
        "idUsuario": 0
      }
      this.mostrarloading = true;
      this.wsProducto.ObtenerBusquedaSugerencia(postData).subscribe(data => {
        this.mostrarloading = false;
        this.OnSuccessCoincidenciasProductos(data);
      }, error => {
        this.mostrarloading = false;
        this.OnErrorCoincidenciasProductos(error);
      });
    }
    
  }

  OnSuccessCoincidenciasProductos(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.productos = data;
    }
    this.FocusTxtBusqueda();
  }

  OnErrorCoincidenciasProductos(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
    this.FocusTxtBusqueda();
  }

  FocusTxtBusqueda() {
    setTimeout(() => {
      document.getElementById("txtBusqueda").focus();
    }, 500);
  }

  OnGoProductos(){
    this.GoProductos(this.txtBusqueda);
  }

  GoProductos(item) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        palabraCoincidencia: item
      }
    };
    this.router.navigate(['productos-busqueda'], navigationExtras);
  }
}
