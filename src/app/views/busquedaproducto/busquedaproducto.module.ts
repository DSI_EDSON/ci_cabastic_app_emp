import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusquedaproductoPageRoutingModule } from './busquedaproducto-routing.module';

import { BusquedaproductoPage } from './busquedaproducto.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusquedaproductoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [BusquedaproductoPage]
})
export class BusquedaproductoPageModule {}
