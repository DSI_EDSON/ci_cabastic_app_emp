import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NuevopickandpackproductosPage } from './nuevopickandpackproductos.page';

describe('NuevopickandpackproductosPage', () => {
  let component: NuevopickandpackproductosPage;
  let fixture: ComponentFixture<NuevopickandpackproductosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevopickandpackproductosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NuevopickandpackproductosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
