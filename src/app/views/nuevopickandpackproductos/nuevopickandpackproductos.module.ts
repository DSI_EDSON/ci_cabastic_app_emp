import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevopickandpackproductosPageRoutingModule } from './nuevopickandpackproductos-routing.module';

import { NuevopickandpackproductosPage } from './nuevopickandpackproductos.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevopickandpackproductosPageRoutingModule,
    ComponentsModule
  ],
  declarations: [NuevopickandpackproductosPage]
})
export class NuevopickandpackproductosPageModule {}
