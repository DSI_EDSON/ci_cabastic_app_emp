import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevopickandpackproductosPage } from './nuevopickandpackproductos.page';

const routes: Routes = [
  {
    path: '',
    component: NuevopickandpackproductosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevopickandpackproductosPageRoutingModule {}
