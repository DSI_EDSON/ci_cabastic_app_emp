import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UtilsService } from '../../utils.service';
import { PickandpackService } from '../../api/pickandpack.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-nuevopickandpackproductos',
  templateUrl: './nuevopickandpackproductos.page.html',
  styleUrls: ['./nuevopickandpackproductos.page.scss'],
})
export class NuevopickandpackproductosPage implements OnInit {

  pickAndPack: any;
  productos: any;

  constructor(
    private wsPickAndPack: PickandpackService,
    private route: ActivatedRoute,
    private router: Router,
    private utils: UtilsService,
    private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.LeerParametros();
  }

  LeerParametros() {
    this.route.queryParams.subscribe(params => {
      if (params && params.pickAndPack) {
        this.pickAndPack = JSON.parse(params.pickAndPack);
        this.MostrarProductos(this.pickAndPack.idPickPackCliente);
      }
    });
  }


  MostrarProductos(idPickPackCliente) {
    this.productos=null;
    let postData = {
      "idPickPackCliente": idPickPackCliente
    }
    this.utils.ShowLoading();
    this.wsPickAndPack.ObtenerSustanciasPickPack(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessMostrarProductos(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorMostrarProductos(error);
    });
  }

  OnSuccessMostrarProductos(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.productos = data;
      this.pickAndPack.costoTotalDolares = data[0].costoTotalDolares;
      this.pickAndPack.costoTotalPesos = data[0].costoTotalPesos;
    }
  }

  OnErrorMostrarProductos(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  //===================== EVENTOS =====================

  async AlertaEliminarProducto(producto: any) {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: 'Desea eliminar el producto: <b>' + producto.sustancia + '</b>',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancelo');
          }
        }, {
          text: 'Eliminar producto',
          handler: () => {
            this.EliminarProducto(producto);
          }
        }
      ]
    });
    await alert.present();
  }

  EliminarProducto(producto) {
    console.log(producto);
    this.utils.ShowLoading();
    let posData = {
      "idRegistro": producto.idRegistro
    };
    this.wsPickAndPack.EliminarSustanciaPickPack(posData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessEliminarProducto(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorEliminarProducto(error);
    });
  }

  OnSuccessEliminarProducto(data) {
    this.utils.alert('Se ha eliminado el producto exitosamente.');
    this.MostrarProductos(this.pickAndPack.idPickPackCliente);
  }

  OnErrorEliminarProducto(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  OnRelizarPickPack() {
    if (this.pickAndPack != null) {
      if (this.productos.length <= 0) {
        this.utils.alert("Agregue por lo menos un producto para el Pick and Pack.");
        return;
      }
      if(this.pickAndPack.nombreMascota == null){
        this.utils.alert("Seleccione una mascota para el Pick and Pack.");
        return;
      }
      if(this.pickAndPack.idDomicilio ==null || this.pickAndPack.idDomicilio <= 0){
        this.utils.alert("Seleccione una mascota para el Pick and Pack.");
        return;
      }

      this.AlertaRealizarPickPack();
    } else {
      this.utils.alert("Ocurrio un problema, intente mas tarde.");
    }
  }

  async AlertaRealizarPickPack() {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: 'Realmente desea realizar el Pick and Pack?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancelo');
          }
        }, {
          text: 'Realizar Pick and Pack',
          handler: () => {
            this.RelizarPickPack();
          }
        }
      ]
    });
    await alert.present();
  }

  RelizarPickPack() {
    let postData = {
      "IdPickPackCliente": this.pickAndPack.idPickPackCliente
    }
    this.utils.ShowLoading();
    this.wsPickAndPack.RegistraPickPack(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessRelizarPickPack(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorRelizarPickPack(error);
    });
  }

  OnSuccessRelizarPickPack(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log("Pedido Realizado:::::" + JSON.stringify(data));
      if (data.status == 200) {
        this.utils.SetPickAndPackEnCurso(null);
        this.utils.alert("Se realizo correctamente el registro del Pick and Pack")
        this.router.navigate(['inicio'], { replaceUrl: true });
      }
      else {
        this.utils.alert(data.mensaje);
      }
    }
  }

  OnErrorRelizarPickPack(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoAddProductos() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        pickAndPack: JSON.stringify(this.pickAndPack)
      }
    };
    this.router.navigate(['productospickandpack'], navigationExtras);
  }

}
