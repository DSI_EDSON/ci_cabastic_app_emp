import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NuevadireccionpedidoPage } from './nuevadireccionpedido.page';

describe('NuevadireccionpedidoPage', () => {
  let component: NuevadireccionpedidoPage;
  let fixture: ComponentFixture<NuevadireccionpedidoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevadireccionpedidoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NuevadireccionpedidoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
