import { Component, OnInit } from '@angular/core';
import { DireccionService } from '../../api/direccion.service';
import { UtilsService } from "../../utils.service";
import { Router,NavigationExtras,ActivatedRoute } from '@angular/router';
import { Platform,NavController } from '@ionic/angular';



@Component({
  selector: 'app-nuevadireccionpedido',
  templateUrl: './nuevadireccionpedido.page.html',
  styleUrls: ['./nuevadireccionpedido.page.scss'],
})
export class NuevadireccionpedidoPage implements OnInit {

  scl: any;
  idDomicilioEntrega = 0;
  idPais: any;
  idEstado: any = 0;
  idMunicipio: any = 0;
  idColonia: any = 0;
  idCodigoPostal: any = 0;
  localidad: any = '';
  pcolonia = '';

  cNombre = true
  cTelefono = true
  cCodigo = true
  cDireccion = true
  cNumeroext = true

  vNombre = ''
  vTelefono = ''
  vCodigo = ''
  vDireccion = ''
  vNumeroext = ''
  vEstado = ''
  vCiudad = ''


  pTelefono = 'Telefono '
  pCodigo = 'Ej. 58000'
  pDireccion = 'Calle'; //y número, apartado postal, c/d'
  pNumeroext = 'Número'
  pEstado = 'Estado/Provincia/Región'
  pCiudad = 'Ciudad'
  pNombre = 'Nombre'

  paises: any;
  objColonias: any = [];
  heightConstant = 0;

  idCliente:any;

  constructor(public platform: Platform,
    public utils: UtilsService,
    public router: Router,
    private route: ActivatedRoute,
    private wsDireccion: DireccionService,
    private navCtrl: NavController) { }

  ngOnInit() {
    this.ObtenerPaises();
    this.LeerParametros();
  }

  inicializar() {
    this.platform.ready().then(() => {
      //this.LeerParametros();
    });
  }

  LeerParametros(){
    this.route.queryParams.subscribe(params => {
      if (params && params.idCliente) {
        this.idCliente = params.idCliente;
      }
    });
  }

  validar(n) {
    switch (n) {
      case 0:
        this.pNumeroext = 'Campo vacio';
        this.cNumeroext = false;
        break;
      case 1:
        this.pDireccion = 'Campo vacio';
        this.cDireccion = false;

        break;
      case 2:
        this.cNombre = false;
        this.pNombre = 'Campo vacio';
        break;
      case 3:
        this.cTelefono = false;
        this.pTelefono = 'Campo vacio';
        break;
      case 4:
        this.cCodigo = false;
        this.pCodigo = 'Campo vacio';

      default:
        console.log("No such day exists!");
        break;
    }
  }

  ObtenerPaises() {
    this.utils.ShowLoading();
    this.wsDireccion.ObtenerPaises().subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerPaises(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerPaises(error);
    });
  }

  OnSuccessObtenerPaises(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log(JSON.stringify(data));
      this.paises = data;
      this.idPais = data[0].idPais;
    }
  }

  OnErrorObtenerPaises(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  ObtenerColonias() {
    let postData = {
      "codigoPostal": this.vCodigo,
      "idPais": this.idPais
    }
    console.log(JSON.stringify(postData));
    this.utils.ShowLoading();
    this.wsDireccion.ObtenerColonias(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerColonias(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerColonias(error);
    });
  }

  OnSuccessObtenerColonias(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log(JSON.stringify(data));
      this.cCodigo = true;
      this.pCodigo = 'Ej. 58000'
      this.objColonias = data;
      this.pcolonia = this.objColonias[0].colonia;
      this.idColonia = this.objColonias[0].idColonia;

      this.vEstado = this.objColonias[0].estado;
      this.vCiudad = this.objColonias[0].municipio;
      this.idEstado = this.objColonias[0].idEstado;
      this.idMunicipio = this.objColonias[0].idMunicipio;
      this.idCodigoPostal = this.objColonias[0].idCodigoPostal
      this.localidad = this.objColonias[0].localidad;
    }
  }

  OnErrorObtenerColonias(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  OnGuardarDireccion(){
    var cont = 0;
    if (this.idPais != 0) { cont++; } else { }
    if (this.idEstado != 0) { cont++; } else { }
    if (this.idMunicipio != 0) { cont++; } else { }
    if (this.idColonia != 0) { cont++; } else { }
    if (this.idCodigoPostal != 0) { cont++; } else { }
    if (this.vNumeroext != '') { cont++; } else { this.validar(0) }
    if (this.vDireccion != '') { cont++; } else { this.validar(1) }
    if (this.vNombre != '') { cont++; } else { this.validar(2) }
    if (this.vTelefono != '') { cont++; } else { this.validar(3) }
    if (this.vCodigo != '') { cont++; } else { this.validar(4) }

    if (cont == 10) {
        this.GuardarDireccion();
    }
    else{
      this.utils.alert('Existe algun campo vacío');
    }
  }

  GuardarDireccion(){
    let postData = {
      "idDomicilioEntrega": this.idDomicilioEntrega,
      "idPais": this.idPais,
      "idEstado": this.idEstado,
      "idMunicipio": this.idMunicipio,
      "idColonia": this.idColonia,
      "idCodigoPostal": this.idCodigoPostal,
      "idUsuario": this.idCliente,//this.utils.pedidoEnCurso.idCliente,
      "noExterior": this.vNumeroext,
      "calle": this.vDireccion,
      "localidad": this.localidad,
      "nombreContacto": this.vNombre,
      "telefonoContacto": this.vTelefono
    }
    console.log(JSON.stringify(postData));
    this.utils.ShowLoading();
    this.wsDireccion.RegistrarDireccionEntrega(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessGuardarDireccion(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorGuardarDireccion(error);
    });
  }

  OnSuccessGuardarDireccion(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log("Respuesta de guardar direccion: " + JSON.stringify(data));
      if(data.status == 200){
        this.utils.alert("Se guardo correctamente la dirección.");
        this.GoBusquedaDireccion();
      }else{
        this.utils.alert("No se logró guardar correctamente la dirección, intente más tarde.");
      }
    }
  }

  OnErrorGuardarDireccion(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  GoBusquedaDireccion() {
    this.navCtrl.back();
    /*let navigationExtras: NavigationExtras = {
      queryParams: {
        pedido: JSON.stringify(this.utils.pedidoEnCurso)
      }
    };
    this.router.navigate(['busquedadireccion'], navigationExtras);*/
  }

}