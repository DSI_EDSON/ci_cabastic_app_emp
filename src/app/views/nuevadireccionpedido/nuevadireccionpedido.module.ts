import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevadireccionpedidoPageRoutingModule } from './nuevadireccionpedido-routing.module';

import { NuevadireccionpedidoPage } from './nuevadireccionpedido.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevadireccionpedidoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [NuevadireccionpedidoPage]
})
export class NuevadireccionpedidoPageModule {}
