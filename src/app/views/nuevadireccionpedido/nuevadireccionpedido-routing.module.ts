import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevadireccionpedidoPage } from './nuevadireccionpedido.page';

const routes: Routes = [
  {
    path: '',
    component: NuevadireccionpedidoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevadireccionpedidoPageRoutingModule {}
