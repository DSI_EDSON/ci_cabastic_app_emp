import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetallepickpackPage } from './detallepickpack.page';

describe('DetallepickpackPage', () => {
  let component: DetallepickpackPage;
  let fixture: ComponentFixture<DetallepickpackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallepickpackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetallepickpackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
