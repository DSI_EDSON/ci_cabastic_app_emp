import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras ,ActivatedRoute} from '@angular/router';
import { UtilsService } from '../../utils.service';
import { PickandpackService } from '../../api/pickandpack.service'
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detallepickpack',
  templateUrl: './detallepickpack.page.html',
  styleUrls: ['./detallepickpack.page.scss'],
})
export class DetallepickpackPage implements OnInit {

  pickPack: any;
  idPickPackClienteERP:any;
  colorEstatus = "green";

  constructor(private wsPickPack: PickandpackService,
    public router: Router,
    private utils: UtilsService,
    private route: ActivatedRoute,
    private alertCtrl:AlertController) { }

  ngOnInit() {
    this.LeerParametros();
  }


  LeerParametros() {
    this.route.queryParams.subscribe(params => {
      if (params && params.idPickPackClienteERP) {
        this.idPickPackClienteERP = params.idPickPackClienteERP;
        this.ObtenerDetallePickPack();
      }
    });
  }

  ObtenerDetallePickPack(){
    let postData = {
      "idPickPackClienteERP" : this.idPickPackClienteERP
    }
    this.utils.ShowLoading();
    this.wsPickPack.ObtenerDetallePickPackCerrado(postData).subscribe(data => {
      this.OnSuccessObtenerDetallePickPack(data);
      this.utils.HideLoading();
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerDetallePickPack(error);
    });
  }

  OnSuccessObtenerDetallePickPack(data) {
    console.log(data[0]);
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontro el pedido seleccionado");
    } else {
      this.pickPack = data[0];
      this.ObtenerColorEstatusPedido();
    }
  }

  OnErrorObtenerDetallePickPack(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  
  ObtenerColorEstatusPedido(){
    if(this.pickPack.idStatus == 1){
      this.colorEstatus = "green";
    };
    if(this.pickPack.idStatus == 0){
      this.colorEstatus = "red";
    };
    if(this.pickPack.idStatus != 1 && this.pickPack.idStatus != 0){
      this.colorEstatus = "blue";
    };
  }

  async AlertaCancelarPickPack() {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: 'Realmente desea cancelar el Pick and Pack?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancelo');
          }
        }, {
          text: 'Cancelar Pick and Pack',
          handler: () => {
            this.CancelarPickaPack();
          }
        }
      ]
    });
    await alert.present();
  }

  CancelarPickaPack() {
    let postData = {
      "idPickPackClienteERP": this.idPickPackClienteERP,
    }
    this.utils.ShowLoading();
    this.wsPickPack.CancelarPickPackCerrado(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessCancelarPickaPack(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorCancelarPickaPack(error);
    });
  }

  OnSuccessCancelarPickaPack(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log("Pedido Cancelado:::::" + JSON.stringify(data));
      if (data.status == 200) {
        this.utils.alert("Pick and Pack cancelado exitosamente.");
        this.router.navigate(['pickandpacks'], { replaceUrl: true });
      } 
      else {
        this.utils.alert(data.mensaje);
      }
    }
  }

  OnErrorCancelarPickaPack(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }
}
