import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetallepickpackPageRoutingModule } from './detallepickpack-routing.module';

import { DetallepickpackPage } from './detallepickpack.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetallepickpackPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DetallepickpackPage]
})
export class DetallepickpackPageModule {}
