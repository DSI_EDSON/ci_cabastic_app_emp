import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetallepickpackPage } from './detallepickpack.page';

const routes: Routes = [
  {
    path: '',
    component: DetallepickpackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetallepickpackPageRoutingModule {}
