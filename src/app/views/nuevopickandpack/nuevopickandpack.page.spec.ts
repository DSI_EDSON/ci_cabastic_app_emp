import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NuevopickandpackPage } from './nuevopickandpack.page';

describe('NuevopickandpackPage', () => {
  let component: NuevopickandpackPage;
  let fixture: ComponentFixture<NuevopickandpackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevopickandpackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NuevopickandpackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
