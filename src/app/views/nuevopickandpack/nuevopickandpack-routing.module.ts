import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevopickandpackPage } from './nuevopickandpack.page';

const routes: Routes = [
  {
    path: '',
    component: NuevopickandpackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevopickandpackPageRoutingModule {}
