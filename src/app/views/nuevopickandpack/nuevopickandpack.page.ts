import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { UtilsService } from '../../utils.service';
import { Platform, AlertController } from '@ionic/angular';
import { PickandpackService } from '../../api/pickandpack.service';
import { DireccionService } from '../../api/direccion.service';
import { visitAll } from '@angular/compiler';

@Component({
  selector: 'app-nuevopickandpack',
  templateUrl: './nuevopickandpack.page.html',
  styleUrls: ['./nuevopickandpack.page.scss'],
})
export class NuevopickandpackPage implements OnInit {

  pickAndPackEnCurso: boolean = false;

  txtBusqueda: string;
  mostrarloading: boolean = false;
  clientes: any = [];

  pickAndPack: any;
  cliente: any;
  direccion: any;
  mascota: any;

  entregaUnica: boolean=false;//0 -->Entrega unica, 1--> Entegra mensual
  entregaMensual:boolean=false;

  constructor(public router: Router,
    private wsPickAndPack: PickandpackService,
    private wsDireccion: DireccionService,
    private utils: UtilsService,
    private platform: Platform,
    private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (this.utils.pickAndPackEnCurso != null) {
      this.pickAndPackEnCurso = true;
      this.ObtenerPickAndPackCliente();
    }
  }

  ObtenerPickAndPackCliente() {
    let postData = {
      "idEmpleado": this.utils.usuario.idUsuario,
      "idCliente": this.utils.pickAndPackEnCurso.idCliente
    }
    this.utils.ShowLoading();
    this.wsPickAndPack.ObtenerPickAndPackCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerDatosPickAndPack(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerDatosPickAndPack(error);
    });
  }

  OnSuccessObtenerDatosPickAndPack(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log("OnSuccessObtenerDatosPickAndPack-->"+JSON.stringify(data[0]));
      this.pickAndPack = data[0];
      this.utils.SetPickAndPackEnCurso(data[0]);
      this.MostrarDatosPickPack();
    }
  }

  OnErrorObtenerDatosPickAndPack(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  MostrarDatosPickPack() {
    this.ObtenerCliente();
    //Se obtiene los datos de la direccion de entrega
    if (this.pickAndPack.idDomicilio > 0)
      this.ObtenerDireccionEntrega();//console.log("Debemos ir por el domicilio registrado");
    if(this.pickAndPack.idMascota > 0)
      this.ObtenerMascota();
    this.entregaUnica = (this.pickAndPack.entregaUnica == 1 ? true : false);
    this.entregaMensual = (this.pickAndPack.entregaMensual == 1 ? true: false);
  }


  ObtenerCliente() {
    this.cliente = {};
    this.cliente.nombres = this.pickAndPack.nombres;
    this.cliente.correo = this.pickAndPack.correo;
    this.cliente.celular = this.pickAndPack.celular;
  }

  ObtenerMascota(){
    this.mascota = {};
    this.mascota.nombreMascota = this.pickAndPack.nombreMascota;
    this.mascota.raza = this.pickAndPack.raza;
    this.mascota.idMascota = this.pickAndPack.idMascota;
  }

  ObtenerDireccionEntrega() {
    let postData = {
      "idDomicilio": this.pickAndPack.idDomicilio
    }
    this.wsDireccion.ObtenerDireccionEntrega(postData).subscribe(data => {
      this.OnSuccessDireccionEntrega(data);
    }, error => {
      this.OnErrorDireccionEntrega(error);
    });
  }

  OnSuccessDireccionEntrega(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.direccion = data;
      //console.log(JSON.stringify(data));
    }
  }

  OnErrorDireccionEntrega(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  ActualizarFrecuenciaPickPack(){
    this.utils.ShowLoading();
    let postData = {
      "idPickPackCliente": this.pickAndPack.idPickPackCliente,
      "idDomicilio" : "0",
      "idMascota" : "0",
      "entregaMensual": this.pickAndPack.entregaMensual,
      "diaEntrega": this.pickAndPack.diaEntrega,
      "entregaUnica": this.pickAndPack.entregaUnica,
    }
    this.wsPickAndPack.ActualizarFrecuenciaPickPack(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessActualizarFrecuenciaPickPack(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorActualizarFrecuenciaPickPack(error);
    });
  }

  OnSuccessActualizarFrecuenciaPickPack(data) {
  }

  OnErrorActualizarFrecuenciaPickPack(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema al actualizar la frecuencia, intente mas tarde.");
  }
  //===================== EVENTOS =====================

  ObtenerCoincidenciasClientes() {
    this.clientes = [];
    if (this.txtBusqueda != '') {
      let postData = {
        "nombre": this.txtBusqueda
      }
      this.mostrarloading = true;
      this.wsPickAndPack.ObtenerCoincidenciasClientes(postData).subscribe(data => {
        this.mostrarloading = false;
        this.OnSuccessObtenerCoincidenciasClientes(data);
      }, error => {
        this.mostrarloading = false;
        this.OnErrorObtenerCoincidenciasClientes(error);
      });
    }
  }

  OnSuccessObtenerCoincidenciasClientes(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.clientes = data;
    }
  }

  OnErrorObtenerCoincidenciasClientes(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  OnSeleccionCliente(cliente) {
    let postData = {
      "idEmpleado": this.utils.usuario.idUsuario,
      "idCliente": cliente.idCliente
    }
    this.utils.ShowLoading();
    this.wsPickAndPack.ObtenerPickAndPackCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessSeleccionCliente(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorSeleccionCliente(error);
    });
  }

  OnSuccessSeleccionCliente(data) {
    //console.log("Pick registrado:::::"+JSON.stringify(data));
    if (data == null || data.length == 0) {
      this.utils.alert("Ocurrio un problema, intente mas tarde.");
    } else {
      this.utils.SetPickAndPackEnCurso(data[0]);
      this.pickAndPackEnCurso = true;
      this.clientes = [];
      this.ObtenerPickAndPackCliente();
    }
  }

  OnErrorSeleccionCliente(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  OnCambioFrecuencia(val) {
    this.entregaUnica = (val == 1 ? true : false);
    this.entregaMensual = (val == 2 ? true: false);
    this.pickAndPack.entregaUnica = (val == 1 ? 1 : 0);
    this.pickAndPack.entregaMensual = (val == 2 ? 1 : 0);
    //this.pickAndPack.diaEntrega = (val == 1 ? 0 : 1);
    this.ActualizarFrecuenciaPickPack();
  }

  CambiarCantidadDiaFrecuencia(){
    this.ActualizarFrecuenciaPickPack();
  }


  async AlertaCancelarPickPack() {
    const alert = await this.alertCtrl.create({
      header: 'Cabastic',
      message: 'Realmente desea cancelar el Pick and Pack en curso?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancelo');
          }
        }, {
          text: 'Cancelar Pick and Pack',
          handler: () => {
            this.CancelarPickPack();
          }
        }
      ]
    });
    await alert.present();
  }

  CancelarPickPack() {
    let postData = {
      "idPickPackCliente": this.pickAndPack.idPickPackCliente,
    }
    this.utils.ShowLoading();
    this.wsPickAndPack.CancelarPickPackEnCurso(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessCancelarPickPack(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorCancelarPickPack(error);
    });
  }

  OnSuccessCancelarPickPack(data) {
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      //console.log("PickPack Cancelado:::::" + JSON.stringify(data));
      if (data.status == 200) {
        this.utils.SetPickAndPackEnCurso(null);
        this.router.navigate(['pickandpacks'], { replaceUrl: true });
      }
      else {
        this.utils.alert(data.mensaje);
      }
    }
  }

  OnErrorCancelarPickPack(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoProductosPickAndPack() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        pickAndPack: JSON.stringify(this.pickAndPack)
      }
    };
    this.router.navigate(['nuevopickandpackproductos'], navigationExtras);
  }

  GoBusquedaDireccion() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        pickAndPack: JSON.stringify(this.pickAndPack)
      }
    };
    this.router.navigate(['busquedadireccion'], navigationExtras);
  }

  GoBusquedaMascota() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        pickAndPack: JSON.stringify(this.pickAndPack)
      }
    };
    this.router.navigate(['busquedamascota'], navigationExtras);
  }

}
