import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NuevamascotapickpackPage } from './nuevamascotapickpack.page';

describe('NuevamascotapickpackPage', () => {
  let component: NuevamascotapickpackPage;
  let fixture: ComponentFixture<NuevamascotapickpackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevamascotapickpackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NuevamascotapickpackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
