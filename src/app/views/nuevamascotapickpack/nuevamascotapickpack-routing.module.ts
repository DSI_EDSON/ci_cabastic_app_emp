import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevamascotapickpackPage } from './nuevamascotapickpack.page';

const routes: Routes = [
  {
    path: '',
    component: NuevamascotapickpackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevamascotapickpackPageRoutingModule {}
