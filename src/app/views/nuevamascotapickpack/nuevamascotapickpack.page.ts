import { Component, OnInit } from '@angular/core';
import { Router ,ActivatedRoute} from '@angular/router';
import { Platform} from '@ionic/angular';
import { MascotaService } from '../../api/mascota.service';
import { UtilsService } from '../../utils.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-nuevamascotapickpack',
  templateUrl: './nuevamascotapickpack.page.html',
  styleUrls: ['./nuevamascotapickpack.page.scss'],
})
export class NuevamascotapickpackPage implements OnInit {

  pTipoMascota = 'Seleccionar'
  cbTipoMascotaError:boolean = false;
  idTipoMascota  = 0;
  txtNombreError = false;
  pNombre = 'Nombre de mascota'
  txtNombre = '';

  txtRazaError= false;
  pRaza = 'Raza de mascota'
  txtRaza = '';

  tiposMascota: any;
  mascota:any;
  datosIncorrectos:boolean= false;


  constructor(
    public wsMascota : MascotaService,
    public platform : Platform,
    public router : Router,
    public route : ActivatedRoute,
    private utils: UtilsService,
    private navCtrl: NavController) { }

  ngOnInit() {
      this.ObtenerTipoMascota();
  }

  LeerParametros(){
    this.route.queryParams.subscribe(params => {
      if (params && params.mascota) {
        this.mascota = JSON.parse(params.mascota);
        this.MostrarMascotaEditar(this.mascota);
      }
    });
  }

  MostrarMascotaEditar(mascota){
    //this.idTipoMascota = 1;//mascota.idTipoMascota;
    this.txtNombre = mascota.nombre;
    this.txtRaza = mascota.raza;
  }

  ValidarDatos(){
      if(this.idTipoMascota <= 0){
        return false;
      }
      if(this.txtNombre==null || this.txtNombre.trim().length <= 0){
        return false;
      }
      if(this.txtRaza==null || this.txtRaza.trim().length <= 0){
        return false;
      }
      return true;
  }

  MostrarErroresInputs(mostrar){
    this.txtNombreError = mostrar;
    this.txtRazaError = mostrar;
    this.cbTipoMascotaError = mostrar;
    this.datosIncorrectos = mostrar;
  }

  OnBlurTxt(){
    if(this.txtNombre.trim().length > 1 || this.txtRaza.trim().length > 1){
      this.MostrarErroresInputs(false);
    }
  }

  ObtenerTipoMascota(){
    this.utils.ShowLoading();
    this.wsMascota.ObtenerTiposMascota().subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerTipoMascota(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerTipoMascota(error);
    });
  }

  OnSuccessObtenerTipoMascota(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("Ocurrio un problema, intente mas tarde.");
      this.router.navigate(['/busquedamascota'])
    } else {
      this.tiposMascota = data;
      this.pTipoMascota = this.tiposMascota[0].descripcion;
      this.idTipoMascota = this.tiposMascota[0].idTipoMascota;
      this.LeerParametros();
    }
  }

  OnErrorObtenerTipoMascota(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GuardarMascota(mascota){
    this.utils.ShowLoading();
    let posData = {
      "idMascota": mascota.idMascota,
      "idUsuario": mascota.idUsuario,
      "nombre": mascota.nombre,
      "raza": mascota.raza,
      "idTipoMascota" : mascota.idTipoMascota
    };
    this.wsMascota.RegistrarActualizarMascota(posData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessGuardarMascota(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorGuardarMascota(error);
    });
  }

  OnSuccessGuardarMascota(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontraron mascotas");
    } else {
      this.utils.alert("Se guardo correctamente su mascota.");
      this.navCtrl.back();
      //this.router.navigate(['/busquedamascota']);
    }
  }

  OnErrorGuardarMascota(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde."+ JSON.stringify(error));
  }

  //Eventos
  OnGuardarMascota(){
    if(this.ValidarDatos()){
      if(this.mascota==null) {
        this.mascota = [];
        this.mascota.idMascota = 0;
      }
      this.mascota.idUsuario = this.utils.pickAndPackEnCurso.idCliente;
      this.mascota.idTipoMascota = this.idTipoMascota;
      this.mascota.nombre = this.txtNombre;
      this.mascota.raza = this.txtRaza;
      this.GuardarMascota(this.mascota);
    }else{
      this.MostrarErroresInputs(true);
    }
  }

}
