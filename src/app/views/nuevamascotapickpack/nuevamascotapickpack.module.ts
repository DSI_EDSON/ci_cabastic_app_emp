import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevamascotapickpackPageRoutingModule } from './nuevamascotapickpack-routing.module';

import { NuevamascotapickpackPage } from './nuevamascotapickpack.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevamascotapickpackPageRoutingModule,
    ComponentsModule
  ],
  declarations: [NuevamascotapickpackPage]
})
export class NuevamascotapickpackPageModule {}
