import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusquedamascotaPageRoutingModule } from './busquedamascota-routing.module';

import { BusquedamascotaPage } from './busquedamascota.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusquedamascotaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [BusquedamascotaPage]
})
export class BusquedamascotaPageModule {}
