import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusquedamascotaPage } from './busquedamascota.page';

const routes: Routes = [
  {
    path: '',
    component: BusquedamascotaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusquedamascotaPageRoutingModule {}
