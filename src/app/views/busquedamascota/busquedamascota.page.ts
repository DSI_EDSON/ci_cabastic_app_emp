import { Component, OnInit } from '@angular/core';
import { MascotaService } from '../../api/mascota.service';
import { PickandpackService } from '../../api/pickandpack.service'
import { Router, ActivatedRoute } from '@angular/router';
import { UtilsService } from '../../utils.service';

@Component({
  selector: 'app-busquedamascota',
  templateUrl: './busquedamascota.page.html',
  styleUrls: ['./busquedamascota.page.scss'],
})
export class BusquedamascotaPage implements OnInit {

  mascotas: any;
  pickAndPack: any;

  constructor(private wsMascota: MascotaService,
    private wsPickPack: PickandpackService,
    public router: Router,
    private route: ActivatedRoute,
    private utils: UtilsService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params && params.pickAndPack) {
        this.pickAndPack = JSON.parse(params.pickAndPack);
        this.ObtenerMascotasClientes(this.pickAndPack.idCliente);
      }
    });
  }

  ObtenerMascotasClientes(idCliente) {
    this.mascotas = [];
    let postData = {
      "idUsuario": idCliente
    }
    this.utils.ShowLoading();
    this.wsMascota.ObtenerMascotasCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerMascotasClientes(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerMascotasClientes(error);
    });
  }

  OnSuccessObtenerMascotasClientes(data) {  
    if (data == null || data.length == 0) {
      //this.utils.alert("No se encontraron coincidencias");
    } else {
      this.mascotas = data;
    }
  }

  OnErrorObtenerMascotasClientes(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  ActualizarMascotaPickPackCliente(idMascota){
    let postData = {
      "idPickPackCliente": this.pickAndPack.idPickPackCliente,
      "idDomicilio" : "0",
      "idMascota" : idMascota,
      "entregaMensual": this.pickAndPack.entregaMensual,
      "diaEntrega": this.pickAndPack.diaEntrega,
      "entregaUnica": this.pickAndPack.entregaUnica,
    }
    //console.log(postData);
    this.utils.ShowLoading();
    this.wsPickPack.ActualizarDirMasPickPack(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessActualizarMascotaPickPackCliente(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorActualizarMascotaPickPackCliente(error);
    });
  }

  OnSuccessActualizarMascotaPickPackCliente(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("No fue posible cambiar la mascota para el pick and pack, intente mas tarde.");
    } else {
      if(data.status==200){
        this.router.navigate(['/nuevopickandpack']);
      }else{
        this.utils.alert("No fue posible cambiar la mascota para el pick and pack, intente mas tarde.");
      }
    }
  }

  OnErrorActualizarMascotaPickPackCliente(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoNuevaMascotaPickPack(){
    this.router.navigate(['/nuevamascotapickpack']);
  }

}
