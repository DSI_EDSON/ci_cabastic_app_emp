import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../api/usuario.service';
import { Router } from '@angular/router';
import { UtilsService } from '../../utils.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  canal = 3;

  txtNumEmpleado:string ="";
  txtNumEmpleadoError : boolean = false;

  txtContrasena:string ="";
  txtContrasenaError : boolean = false;
  txtContrasenaShow : boolean = false;
  iconEye: string = "assets/eye.svg";
  txtContrasenaType:string = "password";

  mensajeValidacion: string = "Credenciales incorrectas";
  datosIncorrectos: boolean = false;

  constructor(
    private wsUsuario:UsuarioService,
    public router : Router,
    private utils : UtilsService) { }

  ngOnInit() {
    this.VerificarSesion();
  }

  ionViewWillEnter(){
    this.VerificarSesion();
  }

  togglePass(){
    if(this.txtContrasenaShow){
      this.txtContrasenaShow = false
      this.txtContrasenaType = 'password'
      this.iconEye = "assets/eye.svg"
    }else{
      this.txtContrasenaShow = true
      this.txtContrasenaType = 'text'
      this.iconEye = "assets/eye1.svg"
    }
  }

  ValidarDatos(){
    if(this.txtNumEmpleado.trim().length == 0){
       return false;
    }
    if(this.txtContrasena.trim().length == 0){
       return false;
    }

    return true;
  }

  MostrarErroresInputs(mostrar){
    this.txtNumEmpleadoError = mostrar;
    this.datosIncorrectos = mostrar;
    this.txtContrasenaError = mostrar;
    this.datosIncorrectos = mostrar;
  }

  OnBlurTxt(){
    if(this.txtNumEmpleado.trim().length > 1 || this.txtContrasena.trim().length > 1){
      this.MostrarErroresInputs(false);
    }
  }

  OnIniciarSesion(){
    if(this.ValidarDatos()){
      this.OnIniciarSesionWs();
    }
    else{
      this.MostrarErroresInputs(true);
    }
  }

  OnIniciarSesionWs(){
    let usuario = {
      "idCanalDigital": this.canal,"email": this.txtNumEmpleado,"contrasena": this.txtContrasena
    }
    this.utils.ShowLoading();
    this.wsUsuario.IniciarSesion(usuario).subscribe(data=>{
      /*let data_ = {
        "nombre" : "Pruebas",
        "numeroEmpleado" : "10",
        "idUsuario" : 1,
        "result" : {"status" : 200}
      };*/
      this.utils.HideLoading();
      this.SuccessIniciarSesion(data);
    },error=>{
      this.utils.HideLoading();
      this.ErrorIniciarSesion(error);
    });
  }

  SuccessIniciarSesion(data){
    if (data[0] != null){
      this.utils.alert(data[0].mensaje);
    }else{
      if(data.result.status==200){
        this.utils.alert("Bienvenido: "+ data.nombre);
        this.utils.SetLogin(data);
        this.router.navigate(['/inicio'], { replaceUrl: true });
      }
    }
  }

  ErrorIniciarSesion(error){
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  VerificarSesion(){
    if(this.utils.usuario!=null){
      this.router.navigate(['/inicio'], { replaceUrl: true });
    }
  }

}
