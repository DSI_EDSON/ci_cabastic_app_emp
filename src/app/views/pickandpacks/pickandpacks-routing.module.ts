import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PickandpacksPage } from './pickandpacks.page';

const routes: Routes = [
  {
    path: '',
    component: PickandpacksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PickandpacksPageRoutingModule {}
