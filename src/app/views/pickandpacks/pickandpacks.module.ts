import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PickandpacksPageRoutingModule } from './pickandpacks-routing.module';

import { PickandpacksPage } from './pickandpacks.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PickandpacksPageRoutingModule,
    ComponentsModule
  ],
  declarations: [PickandpacksPage]
})
export class PickandpacksPageModule {}
