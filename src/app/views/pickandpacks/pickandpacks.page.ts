import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { UtilsService } from '../../utils.service';
import { PickandpackService } from '../../api/pickandpack.service'

@Component({
  selector: 'app-pickandpacks',
  templateUrl: './pickandpacks.page.html',
  styleUrls: ['./pickandpacks.page.scss'],
})
export class PickandpacksPage implements OnInit {

  txtBusqueda: string;
  mostrarloading: boolean = false;
  clientes: any = [];
  clienteSeleccionado: any;
  pickAndPacks: any;
  allPickAndPacks: any;

  permitirBusqueda: boolean = true;

  constructor(
    private wsPickPack: PickandpackService,
    public router: Router,
    private utils: UtilsService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.ObtenerPickPackCerrados();
  }

  ObtenerPickPackCerrados() {
    let postData = {
      "idEmpleado": this.utils.usuario.idUsuario
    }
    this.mostrarloading = true;
    this.wsPickPack.ObtenerPickPackCerrados(postData).subscribe(data => {
      this.mostrarloading = false;
      this.OnSuccessObtenerPickPackCerrados(data);
    }, error => {
      this.mostrarloading = false;
      this.OnErrorObtenerPickPackCerrados(error);
    });
  }

  OnSuccessObtenerPickPackCerrados(data) {
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontraron pedidos");
    } else {
      this.allPickAndPacks = data;
      this.OnFiltrarPedidosClientes();
      //console.log("PickAndPacks cerrados: " + JSON.stringify(data));
    }
  }

  OnErrorObtenerPickPackCerrados(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }


  OnFiltrarPedidosClientes() {
    if (this.permitirBusqueda) {
      if (this.txtBusqueda != null && this.txtBusqueda.trim().length > 0) {
        //ObtenerCoincidenciasClientes(); en caso de ver todos los pedidos de un cliente
        this.FiltrarPedidosClientes();
      } else {
        this.pickAndPacks = this.allPickAndPacks;
      }
      this.ObtenerColores(); // comentar en caso de ver todos los pedidos de un cliente
    }
  }

  FiltrarPedidosClientes() {
    this.mostrarloading = true;
    this.pickAndPacks = this.allPickAndPacks.filter(word => word.nombres.toUpperCase().includes(this.txtBusqueda.toUpperCase()));
    setTimeout(() => {
      this.mostrarloading = false;
    }, 1000);
  }


/*  ===================== EVENTOS Y FUNCIONES =====================*/

  ObtenerColores() {
    this.pickAndPacks.filter(word => word.idStatus == 1).map(function (x) {
      x.color = "green";
    });
    this.pickAndPacks.filter(word => word.idStatus == 3).map(function (x) {
      x.color = "orange";
    });
    this.pickAndPacks.filter(word => word.idStatus == 0).map(function (x) {
      x.color = "red";
    });
    this.pickAndPacks.filter(word => word.idStatus != 1 && word.idStatus != 3 && word.idStatus != 0).map(function (x) {
      x.color = "blue";
    });
  }

  GoNuevaPickAndPack(){
    this.router.navigate(['/nuevopickandpack'])
  }

  GoDetallePickPack(pickpack_){
    //console.log(pickpack_);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        idPickPackClienteERP: pickpack_.idPickPackClienteERP
      }
    };
    this.router.navigate(['detallepickpack'], navigationExtras);
  }

}
