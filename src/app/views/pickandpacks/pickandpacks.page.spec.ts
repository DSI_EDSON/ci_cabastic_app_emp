import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PickandpacksPage } from './pickandpacks.page';

describe('PickandpacksPage', () => {
  let component: PickandpacksPage;
  let fixture: ComponentFixture<PickandpacksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickandpacksPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PickandpacksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
