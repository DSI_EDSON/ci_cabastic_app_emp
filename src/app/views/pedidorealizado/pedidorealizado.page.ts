import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';
import { UtilsService } from "../../utils.service";
import { PedidoService } from '../../api/pedido.service';

@Component({
  selector: 'app-pedidorealizado',
  templateUrl: './pedidorealizado.page.html',
  styleUrls: ['./pedidorealizado.page.scss'],
})

export class PedidorealizadoPage implements OnInit {
  pedido: any = [];
  idPedido: any;
  idCliente:any;

  constructor(public platform: Platform,
    public utils: UtilsService,
    public router: Router,
    private route: ActivatedRoute,
    private wsPedido: PedidoService) { }

  ngOnInit() {
    this.LeerParametros();
  }


  LeerParametros() {
    this.route.queryParams.subscribe(params => {
      if (params && params.idPedido) {
        this.idPedido = params.idPedido;
        this.idCliente = params.idCliente;
        this.ObtenerDetallePedido();
      }
    });
  }


  ObtenerDetallePedido() {
    this.utils.ShowLoading();
    let posData = {
      "idCliente": this.idCliente,
      "idPedido": this.idPedido
    };
    console.log(JSON.stringify(posData));
    this.wsPedido.ObtenerDetallePedido(posData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerDetallePedido(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerDetallePedido(error);
    });
  }

  OnSuccessObtenerDetallePedido(data) {
    if (data == null || data.length == 0) {
      this.pedido = null;
    } else {
      this.pedido = data;
    }
  }

  OnErrorObtenerDetallePedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoInicio() {
    this.router.navigate(['/inicio'])
  }

}
