import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PedidorealizadoPageRoutingModule } from './pedidorealizado-routing.module';

import { PedidorealizadoPage } from './pedidorealizado.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidorealizadoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [PedidorealizadoPage]
})
export class PedidorealizadoPageModule {}
