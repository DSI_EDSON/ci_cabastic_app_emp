import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductosSubdepaPageRoutingModule } from './productos-subdepa-routing.module';

import { ProductosSubdepaPage } from './productos-subdepa.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductosSubdepaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ProductosSubdepaPage]
})
export class ProductosSubdepaPageModule {}
