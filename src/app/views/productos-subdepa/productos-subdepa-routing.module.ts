import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductosSubdepaPage } from './productos-subdepa.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosSubdepaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosSubdepaPageRoutingModule {}
