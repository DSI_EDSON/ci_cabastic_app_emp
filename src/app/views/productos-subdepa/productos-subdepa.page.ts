import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { SubdepartamentoService } from '../../api/subdepartamento.service';
import { UtilsService } from '../../utils.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-productos-subdepa',
  templateUrl: './productos-subdepa.page.html',
  styleUrls: ['./productos-subdepa.page.scss'],
})
export class ProductosSubdepaPage implements OnInit {

  subDepartamento : any;
  nombreDepartamento : any;
  productos:any;

  constructor(
    private wsSubDepto:SubdepartamentoService,
    private route: ActivatedRoute, 
    private router: Router,
    private utils: UtilsService,
    private navCtrl: NavController
    ) { }

  ngOnInit() {
    this.LeerParametros();
  }

  LeerParametros(){
    this.route.queryParams.subscribe(params => {
      if (params && params.subDepartamento) {
        this.subDepartamento = JSON.parse(params.subDepartamento);
        //console.log("***"+JSON.stringify(this.subDepartamento));
        this.nombreDepartamento = params.nombreDepartamento;
        this.ObtenerProductos(this.subDepartamento.idSubDepartamento);
      }
    });
  }

  ObtenerProductos(idSubDepartamento){
    this.utils.ShowLoading();
    let postData = {"idSubDepartamento": idSubDepartamento}
    this.wsSubDepto.ObtenerProductos(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessObtenerProductos(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorObtenerProductos(error);
    });
  }

  OnSuccessObtenerProductos(data){
    if (data == null || data.length == 0) {
      this.utils.alert("No se encontraron productos, intente mas tarde.");
      this.navCtrl.back();
    } else {
      this.productos = data;
    }
  }

  OnErrorObtenerProductos(error){
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  GoDetalleProducto(id){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        idProducto: id
      }
    };
    this.router.navigate(['detalleproducto'], navigationExtras);
  }

}
