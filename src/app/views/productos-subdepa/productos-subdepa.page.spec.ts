import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductosSubdepaPage } from './productos-subdepa.page';

describe('ProductosSubdepaPage', () => {
  let component: ProductosSubdepaPage;
  let fixture: ComponentFixture<ProductosSubdepaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductosSubdepaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductosSubdepaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
