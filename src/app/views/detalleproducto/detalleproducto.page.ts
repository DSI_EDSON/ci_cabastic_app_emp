import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProductoService } from '../../api/producto.service';
import { PedidoService } from '../../api/pedido.service';
import { UtilsService } from '../../utils.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-detalleproducto',
  templateUrl: './detalleproducto.page.html',
  styleUrls: ['./detalleproducto.page.scss'],
})
export class DetalleproductoPage implements OnInit {

  idProducto: any;
  producto: any;
  idColor: any;
  idTalla: any;
  cantidad: any;
  cbxCantidades:any;

  myRatingPromedio = 4;
  tallasColor: any = [];

  constructor(
    private wsProducto: ProductoService,
    private wsPedido: PedidoService,
    private route: ActivatedRoute,
    private router: Router,
    public utils: UtilsService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    this.LeerParametros();
  }

  LeerParametros() {
    this.route.queryParams.subscribe(params => {
      if (params && params.idProducto) {
        this.idProducto = params.idProducto;
        this.MostrarProducto(this.idProducto);
      }
    });
  }

  MostrarProducto(idProducto) {
    this.utils.ShowLoading();
    let postData = { "idProducto": idProducto };
    this.wsProducto.ObtenerProducto(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessMostrarProducto(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorMostrarProducto(error);
    });
  }

  OnSuccessMostrarProducto(data) {
    //console.log("Producto: " + JSON.stringify(data));
    if (data == null) {
      this.utils.alert("No se encontro el producto que esta buscando, intente mas tarde.");
      this.navCtrl.back();
    } else {
      this.producto = data;
      this.cbxCantidades = [];
      for(var i=1; i<=data.existencias; i++) {
        this.cbxCantidades.push(i);
     }
    }
  }

  OnErrorMostrarProducto(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

  OnObtenerTallas() {
    let color: any;
    this.idTalla = -1;
    color = this.producto.colorTalla.filter(word => word.idColor == this.idColor)[0];
    this.tallasColor = color.talla;
  }

  OnAgregarProductoPedido() {
    if (this.ValidarDatosProducto()) {
      this.AgregarProductoPedido();
    }
  }

  ValidarDatosProducto() {
    if ((this.idColor == null || this.idColor <= 0) && this.producto.colorTalla.length > 0) {
      this.utils.alert('Elige una color para este producto')
      return false;
    }
    if ((this.idTalla == null || this.idTalla <= 0) && this.tallasColor.length > 0) {
      this.utils.alert('Elige una talla para este producto')
      return false;
    }
    if (this.cantidad == null || this.cantidad <= 0) {
      this.utils.alert('Elige una cantidad para este producto');
      return false;
    }
    return true;
  }

  AgregarProductoPedido() {
    this.utils.ShowLoading();
    let postData = {
      "idProducto": this.producto.idProducto,
      "idTalla": (this.idTalla == null ? 0 : this.idTalla),
      "idColor": (this.idColor == null ? 0 : this.idColor),
      "cantidad": this.cantidad,
      "idEmpleado": this.utils.usuario.idUsuario,
      "idPedidoCliente": this.utils.pedidoEnCurso.idPedidoCliente
    };
    this.wsPedido.RegistrarProductoPedidoCliente(postData).subscribe(data => {
      this.utils.HideLoading();
      this.OnSuccessAgregarProductoPedido(data);
    }, error => {
      this.utils.HideLoading();
      this.OnErrorAgregarProductoPedido(error);
    });
  }

  OnSuccessAgregarProductoPedido(data) {
    if (data == null || data.status != 200) {
      this.utils.alert("No se logro añadir el producto al pedido, intente mas tarde.");
      this.navCtrl.back();
    } else {
      this.utils.alert("Se agrego correctamente el producto.");
      this.router.navigate(['/nuevopedido']);
    }
  }

  OnErrorAgregarProductoPedido(error) {
    console.log("*****Error: " + JSON.stringify(error));
    this.utils.alert("Ocurrio un problema, intente mas tarde.");
  }

}
