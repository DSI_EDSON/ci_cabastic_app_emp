import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleproductoPageRoutingModule } from './detalleproducto-routing.module';

import { DetalleproductoPage } from './detalleproducto.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleproductoPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DetalleproductoPage]
})
export class DetalleproductoPageModule {}
