import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { UtilsService } from '../app/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  showSplash:boolean = true;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public router : Router,
    private utils:UtilsService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleBlackTranslucent();
      this.splashScreen.hide();
      this.ObtenerUsuarioSesion();
      this.ObtenerPedidoEnCurso();
      this.ObtenerPickPackEnCurso();
      setTimeout(() => {
        this.showSplash = false;
      }, 7000);

      
    });
  }

  async ObtenerUsuarioSesion(){
    let usuario = await this.utils.ObtenerUsuarioSesion();
    /*if(usuario != null){
      this.router.navigate(['/inicio'], { replaceUrl: true });
    }else{
      this.router.navigate(['/login']);
    }
    */
  }

  async ObtenerPedidoEnCurso(){
      this.utils.ObtenerPedidoEnCurso();
  }

  async ObtenerPickPackEnCurso(){
    this.utils.ObtenerPickAndPackEnCurso();
}

}
