import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayerComponent } from './layer/layer.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
declarations: [LayerComponent],
  imports: [
    CommonModule
  ],
   exports: [LayerComponent],
     schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule { }
